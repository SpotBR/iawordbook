package br.com.spotdev.iawordbook;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.mean.AbstractMean;
import br.com.spotdev.iawordbook.mind.mean.MeanType;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.AbstractOnlineDefinition;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.OnlineDefinitionDownloaderRunnable;

public class Word extends AbstractWord {
	
	List<AbstractOnlineDefinition> onlineDefinitions = new ArrayList<>();

	@SuppressWarnings("deprecation")
	public Word(String nome) {
		super(nome);
		List<AbstractMean> means = new ArrayList<>();
		for(MeanType m1 : MeanType.values()) {
			
			if(m1 != MeanType.NONE) {
				AbstractMean mean;
				try {
					mean = m1.getMeanClass().newInstance();
					means.add(mean);
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		this.setMeans(means);
	}
	
	public boolean hasSinonymous(String s) {
		for(String synon : getSynonymous()) {
			if(s.equalsIgnoreCase(synon))
				return true;
		}
		
		return false;
	}
	
	public String getSynonymousToString() {
		String[] array = getSynonymous();
		String data = "";
		
		if(array.length != 0) {
			StringBuilder sb = new StringBuilder(array[0]);
			for(int i = 1; i < array.length; i++) {
				sb.append(" ");
				sb.append(array[i]);
			}
			
			data = sb.toString();
		}
			
		return data;			
	}
	
	public void downloadDefinitionsBackground(Mind mind) {
		OnlineDefinitionDownloaderRunnable or = 
				new OnlineDefinitionDownloaderRunnable(this);
		mind.getOnlineDefinitionDownload().execute(or);
	}
	
	public void downloadDefinitions() throws Exception {
		try {
			for(Class<AbstractOnlineDefinition> aodC : 
				AbstractOnlineDefinition.CLASS_TYPES) {
				Constructor<AbstractOnlineDefinition> constructor = 
						aodC.getDeclaredConstructor(Word.class);
				addDefinition(constructor.newInstance(this));
			}
		} catch (NoSuchMethodException |
				SecurityException | InstantiationException | 
				IllegalAccessException | IllegalArgumentException | 
				InvocationTargetException e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public synchronized List<AbstractOnlineDefinition> getOnlineDefinitions() {
		return onlineDefinitions;
	}

	public synchronized void addDefinition(AbstractOnlineDefinition add) {
		if(!this.containsDefinition(add)) {
			try {
				this.getOnlineDefinitions().add(add);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized boolean containsDefinition(AbstractOnlineDefinition abs) {
		return containsDefinition(abs.getClass());
	}
	
	public synchronized boolean containsDefinition(
			Class<? extends AbstractOnlineDefinition> definitionClass) {
		for(AbstractOnlineDefinition absItem : getOnlineDefinitions()) {
			if(definitionClass == absItem.getClass()) {
				return true;
			}
		}

		return false;
	}


	public AbstractMean getSelectedMean() {
		AbstractMean selected = null;
		if(getType() != MeanType.NONE) {
			for(AbstractMean mean : getMeans()) {
				if(mean.getClass() == this.getType().getMeanClass()) {
					selected = mean;
				}
			}
		}
		return selected;
	}

	@Override public String toString() {
		
		if(getSynonymous().length != 0) {
			return getNome()+" ("+
					getSynonymousToString().replaceAll(" ", "/")+") ";
		}
		return getNome();
	}
	
}
