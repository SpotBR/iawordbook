package br.com.spotdev.iawordbook.database;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

public class ZipUtils {

	private List<File> fileList;
	private String output = null;
	
	public static final String EXTENSION = "tar.gz";

	protected ZipUtils(String folder, String outputFile, 
			List<File> otherFiles) {
		this.output = outputFile;
		fileList = new ArrayList<File>();
		fileList.addAll(otherFiles);
		generateFileList(new File(folder));
	}


	protected void generateFileList(File node) {
		if (node.isFile()) {
			fileList.add(node);
		}

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(new File(node, filename));
			}
		}
	}
	
	public void compress() throws IOException {
		try (OutputStream fo = Files.newOutputStream(Paths.get(output))){
			     OutputStream gzo = new GzipCompressorOutputStream(fo);
			     ArchiveOutputStream o = new TarArchiveOutputStream(gzo); 
		    for (File f : fileList) {
		        ArchiveEntry entry = o.createArchiveEntry(f, 
		        		f.getPath());
		        if (f.isFile()) {
			        o.putArchiveEntry(entry);
		            try (InputStream i = Files.newInputStream(f.toPath())) {
		            	int len;
		            	while((len = i.read()) != -1) {
		            		o.write(len);
		            	}
		            }
		        }
		        try {
		        	o.closeArchiveEntry();
		        }catch(Exception e) {
		        	e.printStackTrace();
		        }
		    }
		    o.finish();
	        o.close();
		}
		System.out.println("Backup: "+output);
	}
	
}