package br.com.spotdev.iawordbook.database;

import java.util.Comparator;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.MeanType;

public class WordTypeComparator implements Comparator<Word>{

	@Override
	public int compare(Word w1, Word w2) {
		MeanType t1 = w1.getType();
		MeanType t2 = w2.getType();

		if(t1 == t2) {
			if(t1 != MeanType.NONE) {
				if(w1.getSelectedMean().isMyChild(w2)) {
					return -1;
				}else if(w2.getSelectedMean().isMyChild(w1)) {
					return 1;
				}else {
					return 0;
				}
			}
			return 0;
		}else if(t1.ordinal() > 
			t2.ordinal()) {
			return -1;
		}else {
			return 1;
		}
	}

}
