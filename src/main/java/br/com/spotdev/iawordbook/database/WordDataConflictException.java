package br.com.spotdev.iawordbook.database;

import br.com.spotdev.iawordbook.Word;

public class WordDataConflictException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6963667782048045383L;

	private String message = null;
	
	public WordDataConflictException(Word w, String conflict1, String conflict2) {
		message = "The data "+conflict1+" conflicted with "+conflict2+" on word "+w.getNome();
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
}
