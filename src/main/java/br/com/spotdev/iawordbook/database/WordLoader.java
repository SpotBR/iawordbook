package br.com.spotdev.iawordbook.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordAlreadyExistsException;
import br.com.spotdev.iawordbook.mind.mean.AbstractMean;
import br.com.spotdev.iawordbook.mind.mean.MeanType;

public class WordLoader {

	public static final Object FILE_THREAD_LOCK = new Object();
	public static final String wordFolder = "words/";
	public static final String backupFolder = "generated_backup/";
	public static final String PARAM_WORD_NAME = "wordName";
	public static final String PARAM_TYPE = "type";
	public static final String PARAM_DESCRIPTION = "description";
	public static final String PARAM_SYNONYMOUS = "synonymous";
	
	public static void readWords(Mind mind) throws 
			FileNotFoundException, IOException, 
			InstantiationException, IllegalAccessException, 
			WordAlreadyExistsException {
		
		synchronized (FILE_THREAD_LOCK) {
			
			File[] wordFiles = getAllFiles(wordFolder);
			for(File file : wordFiles) {
				Properties properties = new Properties();
				FileInputStream fis = new FileInputStream(file);
				InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
				properties.load(isr);
				isr.close();
				System.out.println(file.getAbsolutePath());
				String proType = properties.getProperty(PARAM_TYPE);
				MeanType type = proType.isEmpty() ? 
						MeanType.NONE : MeanType.valueOf(proType);
	
				String wName = properties.getProperty(PARAM_WORD_NAME).toLowerCase();
				Word word = null;
				
				if(mind.contains(wName)) {
					word = mind.getWord(wName);
				} else {
					word = new Word(wName);
					mind.add(word);
				}
				
				word.setFile(file);
				word.setMeanType(type);
				word.setDescription(properties.getProperty(PARAM_DESCRIPTION));
				
				String synonymous = properties.getProperty(PARAM_SYNONYMOUS);
				synonymous = synonymous == null ? "" : synonymous.trim();
				if(!synonymous.isEmpty())
					word.setSynonymous(synonymous.split(" "));
				
				List<AbstractMean> means = word.getMeans();
				
				for(Object v : properties.keySet()) {
					String key = (String)v;
					if(key.startsWith("mean")) {
						String meanName = key.substring(4).toUpperCase();
						MeanType keyMeanType = null;
						try {
							keyMeanType = MeanType.valueOf(meanName);
						}catch(Exception e) {
							continue;
						}
						String meanValue = properties.getProperty(key);
						for(AbstractMean am : means) {
							if(!meanValue.isEmpty() && 
									type.getMeanClass() == am.getClass() &&
									type == keyMeanType) {
								am.setup(mind, meanValue);
							}
						}
					}else if(key.startsWith("param")) {
						String paramName = key.substring(5).toLowerCase();
						String paramValue = properties.getProperty(key);
						if(!paramValue.isEmpty())
							word.addParam(paramName, paramValue);
					}
				}
				
				
			}
		}

	}
	
	public static File[] getAllFiles(String folderName) {
		File folder = new File(folderName);
		List<File> files = new ArrayList<>();
		for(File file : folder.listFiles()) {
			if(file.isDirectory()) {
				File[] childs = getAllFiles(file.getAbsolutePath());
				for(File child : childs) {
					files.add(child);
				}
			}else {
				files.add(file);
			}
		}
		
		return files.toArray(new File[files.size()]);
	}
	
	public static void saveWord(Word w, String[]... moreParams) {
		synchronized (FILE_THREAD_LOCK) {
			
			Properties properties = new Properties();
			
			File file = w.getFile();
			if(file != null) {
				try {
					FileInputStream fis = new FileInputStream(file);
					InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
				
					properties.load(isr);
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				File checkFile = new File(getFileName(w));
				if(!checkFile.getAbsolutePath().equalsIgnoreCase(
						file.getAbsolutePath())) {
					System.out.println(w.getNome().toUpperCase()+" Mudanças de pasta de arquivo: "+
						checkFile.getName()+". - "+file.getAbsolutePath()+" PARA "+
							checkFile.getAbsolutePath());
					try {
						Files.delete(Paths.get(file.getAbsolutePath()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					file = checkFile;
				}
			}else {
				file = new File(getFileName(w));
				try {
					file.createNewFile();
				} catch (IOException e) {e.printStackTrace();}
			}
			w.setFile(file);
			
			String description = w.getDescription();
			properties.setProperty(PARAM_TYPE, w.getType().name());
			properties.setProperty(PARAM_DESCRIPTION, description == null ? "":description);
			properties.setProperty(PARAM_WORD_NAME, w.getNome());
			properties.setProperty(PARAM_SYNONYMOUS, w.getSynonymousToString());
			
			for(AbstractMean am : w.getMeans()) {
				String pName = "mean"+MeanType.getByMean(am);
				String pValue = am.getValue() == null ? "" : am.getValue();
				properties.setProperty(pName, pValue);
			}
			
			if(moreParams[0].length != 0)
				for(String[] param : moreParams) {
					String pName = "param"+capitalize(param[0]);
					String pValue = param[1];
					properties.setProperty(pName, pValue);
				}
			
			try {
				properties.store(new FileOutputStream(file),"For my creator");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static void makeBackup(List<File> otherFiles) {
		synchronized (FILE_THREAD_LOCK) {
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
			String fileName = df.format(new Date())+"."+ZipUtils.EXTENSION;
			File backupFolderInstance = new File(backupFolder);
			if(!backupFolderInstance.exists()) 
				backupFolderInstance.mkdir();		
			
			ZipUtils zip = new ZipUtils(wordFolder, 
					backupFolder+""+fileName, otherFiles);
			try {
				zip.compress();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void makeBackup() {
		makeBackup(new ArrayList<File>());
	}
	
	public static void deleteFile(Word w) throws IOException {
		synchronized (FILE_THREAD_LOCK) {
			if(w.getFile() != null && w.getFile().exists()) {
				try {
					Files.delete(Paths.get(w.getFile().getAbsolutePath()));
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println(getFileName(w));
			}
			File f = new File(getFileName(w));
			if(f.exists())
				f.delete();
		}
	}
	
	private static String getFileName(Word w) {
		return wordFolder+w.getType().name().toLowerCase()+"/"+capitalize(w.getNome());
	}
	
	public static void saveWord(Word w) {
		saveWord(w, new String[0]);
	}
	
	public static String capitalize(String value) {
		return value.substring(0,1).toUpperCase()+value.substring(1,value.length());
	}
	
}
