package br.com.spotdev.iawordbook.database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BackupRule {

	public static final int MODIFICATION_LIMIT = 20;
	public static int COUNT = 0;
	public static List<File> OTHER_FILES = new ArrayList<>();
	
	public static void checkBackup() {
		if(MODIFICATION_LIMIT <= COUNT) {
			WordLoader.makeBackup(OTHER_FILES);
			COUNT = 0;
		}
	}
	
	
}
