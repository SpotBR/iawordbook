package br.com.spotdev.iawordbook;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.com.spotdev.iawordbook.mind.mean.AbstractMean;
import br.com.spotdev.iawordbook.mind.mean.MeanType;

public class AbstractWord {

	private String nome = null;
	private List<AbstractMean> means = new LinkedList<AbstractMean>();
	private Map<String, String> params = new HashMap<>();
	private MeanType meanType = MeanType.NONE;
	private String description = "";
	private File file = null;
	private String[] synonymous = {};
	
	public AbstractWord(String nome) {
		this.nome = nome;
	}

	public String[] getSynonymous() {
		return synonymous;
	}



	public void setSynonymous(String[] synonymous) {
		this.synonymous = synonymous;
	}



	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<AbstractMean> getMeans() {
		return means;
	}
	
	public void setMeans(List<AbstractMean> means) {
		this.means = means;
	}

	public MeanType getType() {
		return meanType;
	}
	
	public void setMeanType(MeanType type) {
		this.meanType = type;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public void addParam(String p, String v) {
		this.params.put(p, v);
	}
	
	public String getParam(String p) {
		return this.params.get(p);
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	
	
	
}
