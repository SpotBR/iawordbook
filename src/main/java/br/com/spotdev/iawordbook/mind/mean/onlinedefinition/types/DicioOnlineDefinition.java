package br.com.spotdev.iawordbook.mind.mean.onlinedefinition.types;

import java.io.IOException;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.AbstractOnlineDefinition;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.Language;

public class DicioOnlineDefinition 
			extends AbstractOnlineDefinition {

	public DicioOnlineDefinition(Word word) throws IOException {
		super(word);
	}

	@Override
	public String getURL() {
		return String.format("http://www.dicio.com.br/%s/", 
				getEncodedWordName());
	}

	@Override
	public String getTitle() {
		return "Word Book - "+getURL();
	}

	@Override
	public String getFormatedHtmlText() {
		
		String result = "<br><br>(Not found)";
		
		if(!isEmpty()) {
			try {
				Element data = getHTML();
				Elements list = data.getElementsByClass("significado textonovo")
						.get(0).getElementsByTag("span");
				
				StringBuilder sb = new StringBuilder("<br><br>");
				sb.append(list.get(0).text().trim());
				for(int i = 1; i < list.size(); i++) {
					sb.append("<br><br>");
					sb.append(list.get(i).text().trim());
				}
				result = sb.toString();
			}catch(Exception e) {
				System.out.println("Erro na URL "+getURL());
				e.printStackTrace();
			}
		}
		
		return result+"<br><br><br>";
	}

	@Override
	public Language getLanguage() {
		return Language.pt_BR;
	}

	@Override
	public boolean isEmpty() {
		return getResponse().statusCode() == 404 || 
				!getHTML().html().contains(
						"<h2 class=\"tit-significado\">Significado de ");
	}

}
