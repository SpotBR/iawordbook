package br.com.spotdev.iawordbook.mind;

import br.com.spotdev.iawordbook.Word;

public class WordAlreadyExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6329537687711262130L;
	private String wordName = null;
	
	public WordAlreadyExistsException(Word w) {
		this.wordName = w.getNome();
	}
	
	@Override
	public String getMessage() {
		return "The word "+wordName+" already exists.";
	}
	
}
