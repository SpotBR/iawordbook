package br.com.spotdev.iawordbook.mind;

import br.com.spotdev.iawordbook.Word;

public class WordHasPatternDependence extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6329537687711262130L;
	private String patternName = null;
	private String childName = null;
	
	public WordHasPatternDependence(Word child, Word pattern) {
		this.childName = child.getNome().toUpperCase();
		this.patternName = pattern.getNome().toUpperCase();
	}
	
	@Override
	public String getMessage() {
		return "The word "+childName+" is child from "+patternName+" and cant be removed.";
	}
	
}
