package br.com.spotdev.iawordbook.mind.mean.onlinedefinition;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import br.com.spotdev.iawordbook.Word;

public abstract class AbstractOnlineDefinition {

	public static final Class<AbstractOnlineDefinition>[] CLASS_TYPES = getClasses();
	
	private static final String TYPES_PACKAGE = 
			"br.com.spotdev.iawordbook.mind.mean.onlinedefinition.types";
	
	private String word = null;
	private Element html = null;
	private Response response = null;
	
	public AbstractOnlineDefinition(Word word) throws IOException {
		this.word = word.getNome().toLowerCase();
//		this.html = downloadHTML();
		this.html = null;
	}
	
	public abstract String getURL();
	public abstract String getTitle();
	public abstract String getFormatedHtmlText();
	public abstract Language getLanguage();
	public abstract boolean isEmpty();

	public Element downloadHTML() throws IOException {
		Connection connection = Jsoup.connect(getURL());
		Element body = null;
		response = connection.ignoreHttpErrors(true).execute();
		
		body = response.parse().body();
		return body;
	}
	
	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Element getHTML() {
		return html;
	}
	
	public String getText() {
		return String.format("<b>%s</b>%s", getTitle(), getFormatedHtmlText());
	}
	
	@Override
	public String toString() {
		return getText();
	}

	public String getWord() {
		return stripAccents(word);
	}

	@SuppressWarnings("unchecked")
	public static Class<AbstractOnlineDefinition>[] getClasses(){
		
	    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    assert classLoader != null;
	    String path = TYPES_PACKAGE.replace('.', '/');
	    Enumeration<URL> resources = null;
		try {
			resources = classLoader.getResources(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    List<File> dirs = new ArrayList<File>();
	    
	    while (resources.hasMoreElements()) {
	        URL resource = resources.nextElement();
	        try {
				dirs.add(new File(URLDecoder.decode(resource.getFile(),"UTF-8")));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	    }
	    
	    ArrayList<Class<AbstractOnlineDefinition>> classes = 
	    		new ArrayList<Class<AbstractOnlineDefinition>>();
	    for (File directory : dirs) {
	        try {
				classes.addAll(findClasses(directory, TYPES_PACKAGE));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	    }
	    return classes.toArray(new Class[classes.size()]);
	}

	@SuppressWarnings("unchecked")
	private static List<Class<AbstractOnlineDefinition>> 
		findClasses(File directory, String packageName) 
			throws ClassNotFoundException {
	    List<Class<AbstractOnlineDefinition>> classes = 
	    		new ArrayList<Class<AbstractOnlineDefinition>>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    
	    File[] files = directory.listFiles();
	    for (File file : files) {
	        if (file.isDirectory()) {
	            assert !file.getName().contains(".");
	            classes.addAll(findClasses(file, packageName + "." + file.getName()));
	        } else if (file.getName().endsWith(".class")) {
	            classes.add((Class<AbstractOnlineDefinition>) 
	            		Class.forName(packageName + '.' + 
	            				file.getName().substring(0, 
	            						file.getName().length() - 6)));
	        }
	    }
	    return classes;
	}
	
	public String getEncodedWordName() {
		try {
			return URLEncoder.encode(getWord().replaceAll(" ", "_"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String stripAccents(String s) 
	{
	    s = Normalizer.normalize(s, Normalizer.Form.NFD);
	    s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
	    return s;
	}
	
}
