package br.com.spotdev.iawordbook.mind.mean.types;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.mean.AbstractMean;

public class EmpiricMean extends AbstractMean {

	private String value = null;
	
	@Override
	public void setup(Mind mind, String value) {
		this.value = value;
	}

	@Override
	public boolean isMyChild(Word w) {
		return false;
	}

	@Override
	public boolean isMeanComplete() {
		return true;
	}

	@Override
	public String getValue() {
		return value;
	}

	
	
	
}
