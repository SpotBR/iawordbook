package br.com.spotdev.iawordbook.mind.mean.debugger;

public enum AmbiguousType {

	PARENT_NAME,
	PARENT_DEFINITION
	
}
