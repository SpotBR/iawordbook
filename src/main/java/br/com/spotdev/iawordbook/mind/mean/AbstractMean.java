package br.com.spotdev.iawordbook.mind.mean;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;

public abstract class AbstractMean {

	private String description = null;
	
	public abstract String getValue();

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isEmpty() {
		return getValue() == null || getValue().isEmpty();
	}
	
	public Word getWord(Mind mind) {
		for(Word w : mind.getWords()) {
			if(w.getSelectedMean() == this)
				return w;
		}
		return null;
	}
	
	public abstract boolean isMeanComplete();
	
	public abstract void setup(Mind mind, String value);

	public abstract boolean isMyChild(Word w);
	
}
