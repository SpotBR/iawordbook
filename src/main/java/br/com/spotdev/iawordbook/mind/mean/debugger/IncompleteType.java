package br.com.spotdev.iawordbook.mind.mean.debugger;

public enum IncompleteType {

	NO_MEAN("Word does not have mean."),
	NO_MEAN_TYPE("No mean type."),
	INCOMPLETE_MEANS("Incomplete means.");
	
	private String message = null;
	
	private IncompleteType(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
