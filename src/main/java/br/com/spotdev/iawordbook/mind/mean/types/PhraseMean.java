package br.com.spotdev.iawordbook.mind.mean.types;

import java.util.LinkedList;
import java.util.List;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.AbstractDefinitionMean;

public class PhraseMean extends AbstractDefinitionMean {

	private LinkedList<Word> wordList = new LinkedList<>();

	@Override
	public List<Word> getWordList() {
		return wordList;
	}

	@Override
	public void clear() {
		wordList.clear();
	}

	@Override
	public void addWordDefinition(Word w, int index) {
		wordList.add(w);
	}
}
