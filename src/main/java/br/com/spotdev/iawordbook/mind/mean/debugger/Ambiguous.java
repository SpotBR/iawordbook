package br.com.spotdev.iawordbook.mind.mean.debugger;

import java.util.List;

import br.com.spotdev.iawordbook.Word;

public class Ambiguous {

	private AmbiguousType ambiguousType = null;
	private String wordName = null;
	private List<Word> ambiguousTrace = null;
	
	public Ambiguous(AmbiguousType ambiguousType, String wordName, 
			List<Word> ambiguousTrace) {
		this.ambiguousType = ambiguousType;
		this.wordName = wordName;
		this.ambiguousTrace = ambiguousTrace;
	}
	
	public String getMessage() {
		String firstParent = ambiguousTrace.get(0).getNome().toUpperCase();
		String lastParent = ambiguousTrace.get(ambiguousTrace.size()-1).
				getNome().toUpperCase();
		wordName = wordName.toUpperCase();
		String message = null;
		if(getAmbiguousType() == AmbiguousType.PARENT_DEFINITION) {
			message = 
					String.format("The word \"%s\" used to define your parent \n"
							+ "%s can not be used to define your child \"%s\" \n"
							+ " or will be redundant to use the word \"%s\" \n"
							+ "to define \"%s\" since it is already contained \n"
							+ "explicitly in \"%s\" definition and now implicitly in \n"
							+ "\"%s\".", 
							wordName, lastParent, firstParent, wordName, 
							lastParent, lastParent, firstParent);
		}else {
			message = 
					String.format("The word %s cant be its own parent.", 
							wordName);
		}
		message+="\n\nTRACE:\n";
		message+=wordName+" is defining "+ambiguousTrace.get(0).getNome().toUpperCase();
		for(int i = 1; i < ambiguousTrace.size()-1; i++) {
			message+=" that defines "+ambiguousTrace.get(i).getNome().toUpperCase();
		}
		
		String last = ambiguousTrace.get(ambiguousTrace.size()-1).getNome().toUpperCase();
		if(getAmbiguousType() == AmbiguousType.PARENT_DEFINITION) {
			message+=" that defines "+last+" and it is defined by "+getWordName()+" again.";
		}else {
			message+=" that defines again "+last;
		}
		
		return message;

	}

	public AmbiguousType getAmbiguousType() {
		return ambiguousType;
	}

	public void setAmbiguousType(AmbiguousType ambiguousType) {
		this.ambiguousType = ambiguousType;
	}

	public String getWordName() {
		return wordName;
	}

	public void setWordName(String wordName) {
		this.wordName = wordName;
	}

	public List<Word> getAmbiguousTrace() {
		return ambiguousTrace;
	}

	public void setAmbiguousTrace(List<Word> ambiguousTrace) {
		this.ambiguousTrace = ambiguousTrace;
	}
	
}
