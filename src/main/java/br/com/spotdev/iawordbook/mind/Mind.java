package br.com.spotdev.iawordbook.mind;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.database.WordLoader;
import br.com.spotdev.iawordbook.database.WordTypeComparator;
import br.com.spotdev.iawordbook.mind.mean.AbstractDefinitionMean;
import br.com.spotdev.iawordbook.mind.mean.debugger.Ambiguous;
import br.com.spotdev.iawordbook.mind.mean.debugger.AmbiguousType;

public class Mind {

	private List<Word> wordList = new ArrayList<>();
	private MindListener mindListener = null;
	private ExecutorService onlineDefinitionDownload = 
			Executors.newFixedThreadPool(10);
	
	public void add(Word word) throws WordAlreadyExistsException {

		synchronized (WordLoader.FILE_THREAD_LOCK) {
				if(contains(word.getNome())) {
					throw new WordAlreadyExistsException(word);
				}
				
				wordList.add(word);
				wordList.sort(new WordTypeComparator());
				
				if(mindListener != null)
					mindListener.addWordEvent(word);
		
				word.downloadDefinitionsBackground(this);
		}
	}
	
	public Word getWord(String name) {
		for(Word w : getWords()) {
			if(w.getNome().equalsIgnoreCase(name))
				return w;
		}
		
		return null;
	}
	
	public Word getWordBySynonymous(String syn) {
		for(Word w : getWords()) {
			for(String s : w.getSynonymous())
				if(s.equalsIgnoreCase(syn))
					return w;
		}
		
		return null;
	}
	
	public void removeWord(Word word) throws WordHasPatternDependence, 
		IOException {
		synchronized (WordLoader.FILE_THREAD_LOCK) {
			Word pattern[] = getPattern(word);
			if(pattern.length != 0)
				throw new WordHasPatternDependence(word, pattern[0]);

			WordLoader.deleteFile(word);
			wordList.remove(word);
			
			if(mindListener != null)
				mindListener.removedWordevent(word);
			
			word.setFile(null);
	
		}
	}
	
	public Word[] getPattern(Word word) {
		List<Word> parent = new ArrayList<>();
		for(Word w : getWords()) {
			if(w.getSelectedMean() instanceof AbstractDefinitionMean) {
				AbstractDefinitionMean p = (AbstractDefinitionMean) w.getSelectedMean();
				if(p.getWordList().contains(word)) {
					parent.add(w);
				}
			}
		}
		
		return parent.toArray(new Word[parent.size()]);
	}
	
	/**
	 * 
	 * @param ignoreWord Word to ignore in search
	 * @param synonymou Synonymous to search
	 * @return Return if exists synonimous
	 */
	public boolean existsSynonymous(Word ignoreWord, String synonymou) {
		for(Word word : getWords()) {
			
			if(ignoreWord == word)
				continue;
			
			if(word.getNome().equalsIgnoreCase(synonymou))
				return true;
			
			for(String s : word.getSynonymous()) {
				if(synonymou.equalsIgnoreCase(s.trim())) {
					return true;
				}
			}
			
		}
		return false;
	}
	
	public boolean contains(String wordName) {
		for(Word w : getWords()) {
			if(w.getNome().equalsIgnoreCase(wordName))
				return true;
			
			for(String s : w.getSynonymous()) {
				if(wordName.equalsIgnoreCase(s.trim())) {
					return true;
				}
			}
			
		}
		return false;
	}
	
	public Ambiguous isAmbiguous(String newWord, Word inParent) {
		List<Word> ambiguousTrace = new ArrayList<>();
		ambiguousTrace.add(inParent);
		
		AmbiguousType type = null;
		Word[] parents = getPattern(inParent);
		
		for(int i = 0; i < parents.length; i++) {
			inParent = parents[i];
			while(inParent != null) {
				ambiguousTrace.add(inParent);
				if(inParent.getNome().equalsIgnoreCase(newWord)) {
					type = AmbiguousType.PARENT_NAME;
					break;
				}
				
				AbstractDefinitionMean pm = (AbstractDefinitionMean) inParent.getSelectedMean();
				for(Word w : pm.getWordList()) {
					if(w.getNome().equalsIgnoreCase(newWord)) {
						type = AmbiguousType.PARENT_DEFINITION;
					}
				}
	
				inParent = getPattern(inParent).length == 0 ? 
						null : getPattern(inParent)[0];
			}
			
			if(type != null)
				break;
		}
		
		if(type != null) {
			Ambiguous am = new Ambiguous(type, newWord, ambiguousTrace);
			return am;
		}else
			return null;
		
	}
	
	public boolean contains(Word w) {
		synchronized (WordLoader.FILE_THREAD_LOCK) {
			return wordList.contains(w);
		}
	}
	
	public synchronized Word[] getWords() {
		wordList.sort(new WordTypeComparator());
		return wordList.toArray(new Word[wordList.size()]);
	}
	
	public void setMindListener(MindListener mindListener){
		this.mindListener = mindListener;
	}

	public ExecutorService getOnlineDefinitionDownload() {
		return onlineDefinitionDownload;
	}
	
	
}
