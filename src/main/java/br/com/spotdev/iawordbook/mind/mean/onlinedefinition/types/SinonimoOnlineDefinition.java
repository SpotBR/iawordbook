package br.com.spotdev.iawordbook.mind.mean.onlinedefinition.types;

import java.io.IOException;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.AbstractOnlineDefinition;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.Language;

public class SinonimoOnlineDefinition 
			extends AbstractOnlineDefinition {

	public SinonimoOnlineDefinition(Word word) throws IOException {
		super(word);
	}

	@Override
	public String getURL() {
		return String.format("https://www.sinonimos.com.br/%s/", 
				getEncodedWordName());
	}

	@Override
	public String getTitle() {
		return "Synonymous - "+getURL();
	}

	@Override
	public String getFormatedHtmlText() {
		
		String result = "<br><br>(Not found)<br><br>";
		
		if(!isEmpty()) {
			Element data = getHTML();
			Elements list = data.getElementById("content").
					getElementsByClass("s-wrapper");
			
			StringBuilder sb = new StringBuilder("<br><br>");
			sb.append(list.get(0).text().trim());
			for(int i = 1; i < list.size(); i++) {
				sb.append("<br><br>");
				sb.append(list.get(i).text().trim());
			}
			result = sb.toString();
		}
		
		return result+"<br><br><br>";
	}

	@Override
	public Language getLanguage() {
		return Language.pt_BR;
	}

	@Override
	public boolean isEmpty() {
		return getResponse().statusCode() == 404 || 
				!getHTML().html().contains("<h1 class=\"h-palavra\">Sinônimo de ");
	}

}
