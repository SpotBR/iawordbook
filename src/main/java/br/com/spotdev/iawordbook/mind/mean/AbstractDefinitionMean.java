package br.com.spotdev.iawordbook.mind.mean;

import java.util.ArrayList;
import java.util.List;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordAlreadyExistsException;
import br.com.spotdev.iawordbook.mind.mean.debugger.Incomplete;
import br.com.spotdev.iawordbook.mind.mean.debugger.IncompleteType;

public abstract class AbstractDefinitionMean extends AbstractMean {

	@Override
	public String getValue() {
		if(getWordList().size() == 0)
			return null;

		StringBuilder sb = new StringBuilder();
		sb.append(getWordList().get(0).getNome());
		for(int i = 1; i < getWordList().size(); i++) {
			sb.append(" ");
			sb.append(getWordList().get(i).getNome());
		}
		
		return sb.toString();
	}

	@Override
	public boolean isMeanComplete() {
		
		if(getWordList().size() == 0)
			return false;
		
		for(Word w : getWordList()) {
			if(w.getType() == MeanType.NONE || 
					(w.getSelectedMean().isEmpty() && 
					w.getType() != MeanType.EMPIRIC)) 
				return false;
		}
		
		return true;
	}
	
	public abstract void clear();
	public abstract void addWordDefinition(Word w, int index);

	@Override public void setup(Mind mind, String value) {
		clear();
		String word[] = value.split(" ");
		
		for(int i = 0; i < word.length; i++) {
			
			if(word[i].isEmpty())
				continue;
			
			Word w = null;
			if(mind.contains(word[i])) {
				w = mind.getWord(word[i]);
			}else {
				w = new Word(word[i]);
				try {
					mind.add(w);
				} catch (WordAlreadyExistsException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
			addWordDefinition(w, i);
		}

		// Pegando palavras da mente e verificando se é do tipo frase para 
		// analisar se é pai desta palavra, e logo, tentar ser reinstalado
		Word[] words = mind.getWords();
		Word thisWord = getWord(mind);
		for(Word w : words) {
			if(w.getSelectedMean() instanceof AbstractDefinitionMean 
					&& w != thisWord) {
				AbstractDefinitionMean pmw = (AbstractDefinitionMean)w.getSelectedMean();
				if(pmw.isMyChild(thisWord)) {
					pmw.setup(mind, pmw.getValue());
				}
			}
		}
	}


	@Override
	public boolean isMyChild(Word w) {

		if(getWordList().contains(w))
			return true;
		
		
		return false;
	}
	
	public abstract List<Word> getWordList();
	
	public Incomplete[] getIncompletes() {		
		List<Incomplete> iList = new ArrayList<>();
		
		for(Word w : getWordList()) {
			if(w.getType() == MeanType.NONE) 
				iList.add(new Incomplete(w,IncompleteType.NO_MEAN_TYPE));
			else if(w.getSelectedMean().isEmpty() &&
					w.getType() == MeanType.PHRASE &&
					w.getType() == MeanType.POLE){
				iList.add(new Incomplete(w,IncompleteType.NO_MEAN));
			}
			else if(!w.getSelectedMean().isMeanComplete()){
				iList.add(new Incomplete(w,IncompleteType.INCOMPLETE_MEANS));
			}
		}
		
		return iList.toArray(new Incomplete[iList.size()]);
	}

}
