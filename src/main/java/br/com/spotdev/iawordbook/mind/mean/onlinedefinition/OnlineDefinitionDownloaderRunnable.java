package br.com.spotdev.iawordbook.mind.mean.onlinedefinition;

import br.com.spotdev.iawordbook.Word;

public class OnlineDefinitionDownloaderRunnable implements Runnable {

	private Word w = null;
	
	public OnlineDefinitionDownloaderRunnable(Word w) {
		this.w = w;
	}
	
	@Override
	public void run() {
		try {
			w.downloadDefinitions();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
