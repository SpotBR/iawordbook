package br.com.spotdev.iawordbook.mind.mean.debugger;

import br.com.spotdev.iawordbook.Word;

public class Incomplete {

	private Word word = null;
	private IncompleteType type = null;
	
	public Incomplete(Word word, IncompleteType type) {
		this.word = word;
		this.type = type;
	}
	
	public Word getWord() {
		return word;
	}
	public void setWord(Word word) {
		this.word = word;
	}
	public IncompleteType getType() {
		return type;
	}
	public void setType(IncompleteType type) {
		this.type = type;
	}
	
	public String getMessage() {
		return type.getMessage();
	}
	
}
