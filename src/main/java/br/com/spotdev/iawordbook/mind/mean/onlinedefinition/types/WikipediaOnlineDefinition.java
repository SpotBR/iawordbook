package br.com.spotdev.iawordbook.mind.mean.onlinedefinition.types;

import java.io.IOException;

import org.jsoup.nodes.Element;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.AbstractOnlineDefinition;
import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.Language;

public class WikipediaOnlineDefinition 
			extends AbstractOnlineDefinition {

	public WikipediaOnlineDefinition(Word word) throws IOException {
		super(word);
	}

	@Override
	public String getURL() {
		System.out.println(String.format("https://pt.m.wikipedia.org/wiki/%s", 
				getEncodedWordName()));
		return String.format("https://pt.m.wikipedia.org/wiki/%s", 
				getEncodedWordName());
	}

	@Override
	public String getTitle() {
		return "Wikipedia - "+getURL();
	}

	@Override
	public String getFormatedHtmlText() {
		
		String result = "<br><br>(Not found)<br><br>";
		if(!isEmpty()) {
			Element data = getHTML();
			Element list = data.getElementById("content");
			
			return list.html();
		}
		
		return result+"<br><br><br>";
	}

	@Override
	public Language getLanguage() {
		return Language.pt_BR;
	}

	@Override
	public boolean isEmpty() {
		return getResponse().statusCode() == 404;
	}

}
