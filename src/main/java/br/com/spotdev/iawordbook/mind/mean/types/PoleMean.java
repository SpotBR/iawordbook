package br.com.spotdev.iawordbook.mind.mean.types;

import java.util.ArrayList;
import java.util.List;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.AbstractDefinitionMean;

public class PoleMean extends AbstractDefinitionMean {

	private Word wrapped = null;
	private Word wrap = null;
	
	public Word getWrapped() {
		return wrapped;
	}

	public void setWrapped(Word wrapped) {
		this.wrapped = wrapped;
	}

	public Word getWrap() {
		return wrap;
	}

	public void setWrap(Word wrap) {
		this.wrap = wrap;
	}

	@Override
	public List<Word> getWordList(){
		List<Word> values = new ArrayList<>();
		if(wrap != null)
			values.add(wrap);
		
		if(wrapped != null)
			values.add(wrapped);
		return values;
	}
	
	@Override
	public void clear() {
		this.setWrap(null);
		this.setWrapped(null);
	}

	@Override
	public void addWordDefinition(Word w, int index) {
		
		if(index == 0)
			this.setWrap(w);
		else
			this.setWrapped(w);
	}
}
