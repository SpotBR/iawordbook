package br.com.spotdev.iawordbook.mind.mean;

import br.com.spotdev.iawordbook.mind.mean.types.*;

public enum MeanType {
	
	NONE(null),
	EMPIRIC(EmpiricMean.class),
	PHRASE(PhraseMean.class),
	POLE(PoleMean.class);
	
	private Class<? extends AbstractMean> mean = null;
	
	private MeanType(Class<? extends AbstractMean> mean) {
		  this.mean = mean;
	}

	public Class<? extends AbstractMean> getMeanClass() {
		return mean;
	}
	
	public static MeanType getByMean(AbstractMean am) {
		for(MeanType mt : values()) {
			if(mt.getMeanClass() == am.getClass())
				return mt;
		}
		
		return null;
	}
	
	
}
