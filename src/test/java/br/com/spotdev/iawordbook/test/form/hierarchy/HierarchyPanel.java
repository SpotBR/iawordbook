package br.com.spotdev.iawordbook.test.form.hierarchy;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.database.BackupRule;
import br.com.spotdev.iawordbook.database.WordLoader;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordAlreadyExistsException;
import br.com.spotdev.iawordbook.mind.WordHasPatternDependence;
import br.com.spotdev.iawordbook.mind.mean.AbstractDefinitionMean;
import br.com.spotdev.iawordbook.mind.mean.MeanType;

import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.Home;
import br.com.spotdev.iawordbook.test.form.wordcomponents.AbstractDefinitionWordGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordBlock;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.EmpiricGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.NoMeanGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.NoteInternalFrame;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.PhraseGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.PoleGraphic;

public class HierarchyPanel extends JDesktopPane 
	implements MouseMotionListener, MouseListener {

	private static final long serialVersionUID = -8574917018859524203L;
	
	private boolean isNinbus = false;
	private Mind mind = null;
	
	private JPopupMenu createWordPopup = null;
	private Point lastPopupPosition = null;
	
	public HierarchyPanel(Mind mind) {
		this.mind = mind;
		this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		this.setBackground(Color.WHITE);
		
		createWordPopup = new JPopupMenu();
		JMenuItem createWordItem = new JMenuItem("+ Create word");
		createWordItem.setCursor(new Cursor(Cursor.HAND_CURSOR));
		createWordItem.addActionListener(this::onPressCreateWordMenuItem);
		createWordPopup.add(createWordItem);
		
		JMenuItem createNoteItem = new JMenuItem("+ Create note");
		createNoteItem.setCursor(new Cursor(Cursor.HAND_CURSOR));
		createNoteItem.addActionListener(this::onPressCreateNoteMenuItem);
		createWordPopup.add(createNoteItem);

		this.add(createWordPopup);

		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.setFocusable(true);
		
		// Define as posições dos objetos
		Word[] words = mind.getWords();
		for(Word w : words) {
			addWordGraphic(w);
		}

		// Código para recuperar posição das notas
		for(NoteInternalFrame note : NoteInternalFrame.loadAll()) {
			this.add(note);
			note.setVisible(true);
		}
		refreshSize();
	}

	public void refreshWordGraphic(WordGraphic wg) {
		Point p = wg.getLocation();
		Word w = wg.getWord();
		remove(wg);
		WordGraphic newWg = addWordGraphic(w);
		newWg.setLocation(p);
		WordLoader.saveWord(w);
	}
	
	public void refreshSize() {
		for(WordGraphic wg : getWordGraphics()) {
			checkSize(wg);
		}
		
		for(NoteInternalFrame nt : getNotes()) {
			checkSize(nt);
		}
	}
	
	public void removeWordAndGraphic(HierarchyInternalFrame j) 
			throws WordHasPatternDependence, IOException {
		
		if(j instanceof WordGraphic) {
			WordGraphic wg = (WordGraphic) j;
			Word pattern[] = mind.getPattern(wg.getWord());
			if(pattern.length != 0)
				throw new WordHasPatternDependence(wg.getWord(), pattern[0]);
	
			IAWordBookEditor.HOME_PANEL.clearSelected();
		}
		
		try {
			j.removeSavedData();
		} catch (WordHasPatternDependence e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		remove(j);
	}
	
	@Override
	public void remove(Component j) {
		super.remove(j);
		
		if(j instanceof JInternalFrame) {
			((JInternalFrame) j).dispose();
		}
	}
	
	public void onPressCreateWordMenuItem(ActionEvent event) {
		showCreateWordDialog(createWordPopup);
	}
	
	public void onPressCreateNoteMenuItem(ActionEvent event) {
		NoteInternalFrame note = new NoteInternalFrame();
		note.setLocation(lastPopupPosition);
		this.add(note);
		note.setVisible(true);
	}
	
	public void showCreateWordDialog(Component component) {
		String word = null;
		while(true) {
			word = JOptionPane.showInputDialog(component, 
					"Insert the word name that you want to create:", 
					"Create word",
					JOptionPane.PLAIN_MESSAGE);
			
			if(word == null)
				break;

			word = word.trim();
			if(!word.matches("[a-zA-ZÀ-ú]+")) {
				JOptionPane.showMessageDialog(component, 
						"The word can compose just letters without spaces.", 
						"Create word", JOptionPane.ERROR_MESSAGE);
				continue;
			}
			
			if(mind.contains(word)) {
				JOptionPane.showMessageDialog(component, 
						"This word already exists on your database.", 
						"Create word", JOptionPane.ERROR_MESSAGE);
				continue;
			}
			
			break;
		}
		
		if(word != null) {
			try {
				BackupRule.checkBackup();
				createWord(word, (int)lastPopupPosition.getX(), 
						(int)lastPopupPosition.getY());
			} catch (WordAlreadyExistsException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createWord(String word, int x, int y) throws WordAlreadyExistsException {
		word = word.toLowerCase();
		Word w = new Word(word);
		mind.add(w);
		WordGraphic wg = addWordGraphic(w);
		wg.setLocation(x, y);
		
		IAWordBookEditor.HOME_PANEL.setSelectedWord(w);
	}
	
	public Point resetChildPosition(WordGraphic childWg, Point reference) {
		
		WordGraphic wg = 
				getWordGraphicsByWord(mind.getPattern(childWg.getWord())[0]);
		AbstractDefinitionMean pm = (AbstractDefinitionMean) wg.getWord().getSelectedMean();
		
		int x = 0;
		int y = 0;
		int width = childWg.getWidth();
		
		if(reference != null) {
			x = (int) (reference.getX()+5);
			y = (int) (reference.getY());
		}else {
			x = wg.getX()+(wg.getWidth()/2)-
					((pm.getWordList().size()*width)/2);
			y = wg.getY()+90;
		}
		
		if(x < 0) x = 0; if(y < 0) y = 0;
		reference = new Point(x, y);
		childWg.setLocation(reference);
		reference.setLocation(x+childWg.getWidth(), y);
		
		return reference;  
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		if(isNinbus) {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		
		int lineSpace = 40;
		g.setColor(new Color(204, 246, 252));
		for(int i = lineSpace; i < getWidth()/lineSpace*lineSpace; i+=lineSpace) {
			g.drawLine(i, 0, i, getHeight());
		}
		for(int i = lineSpace; i < getHeight()/lineSpace*lineSpace; i+=lineSpace) {
			g.drawLine(0, i, getWidth(), i);
		}

	
		g.setColor(Color.BLACK);
		// Desenha eles na tela	
		for(WordGraphic wg : getWordGraphics()) {
			drawLine(wg, g);
		}
			
		//TODO Verificar a necessidade do repaint
		//repaint();
	}
	
	public WordGraphic addWordGraphic(Word w) {
		
		if(existsWordGraphic(w))
			return null;
		
		WordGraphic wg = null;
		MeanType type = w.getType();
		switch (w.getType()) {
		case POLE:
		case PHRASE:
			AbstractDefinitionWordGraphic pg = type == MeanType.POLE ? 
					new PoleGraphic(w) : new PhraseGraphic(w);
					
			wg = pg;
			AbstractDefinitionMean pm = (AbstractDefinitionMean) w.getSelectedMean();
			for(Word child : pm.getWordList()) {
				addWordGraphic(child);
			}
			break;
		case EMPIRIC:
			EmpiricGraphic em = new EmpiricGraphic(w);
			wg = em;
			break;
		default:
			NoMeanGraphic nm = new NoMeanGraphic(w);
			wg = nm;
		}
		
		this.add(wg);
		
		if(wg.hasPosParam())
			wg.setLocation(wg.getPosParam());
		
		wg.setVisible(true);
		checkSize(wg);
		
		return wg;
	}
	
	public void drawLine(WordGraphic wg, Graphics g) {
		Word w = wg.getWord();
		if(w.getSelectedMean() instanceof AbstractDefinitionMean){
			
			AbstractDefinitionWordGraphic pg = (AbstractDefinitionWordGraphic) wg;
			AbstractDefinitionMean pm = (AbstractDefinitionMean) w.getSelectedMean();
			
			for(Word childW : pm.getWordList()) {
				if(existsWordGraphic(childW)) {
					WordGraphic wgchild = getWordGraphicsByWord(childW);
					WordBlock meanWb = pg.getBlockByWord(childW);
					
					if(wgchild.getWord().getNome().equalsIgnoreCase("e") ||
							wgchild.getWord().getNome().equalsIgnoreCase("de") ||
							wgchild.getWord().getNome().equalsIgnoreCase("do"))
						continue;
					
					Point hP = this.getLocationOnScreen();
					Point childP = wgchild.getLocationOnScreen();
					Point meanP = meanWb.getLocationOnScreen();
					
					Point childPR = new Point((int)((childP.getX()-hP.getX())+wgchild.getWidth()/2),
							((int)(childP.getY()-hP.getY())));
					Point meanPR = new Point((int)(meanP.getX()-hP.getX())+meanWb.getWidth()/2,
							(int)(meanP.getY()-hP.getY())+meanWb.getHeight());
					
					
					g.setColor(Color.BLACK);						
					g.drawLine((int)childPR.getX(), (int)childPR.getY(), 
							(int)meanPR.getX(), (int)meanPR.getY());
				}
			}
		}
	}
	
	public boolean checkSize(JInternalFrame jf) {
		
		if(jf.getX() < 0) {
			jf.setLocation(0, jf.getY());
		}
		
		
		if(jf.getY() < 0) {
			jf.setLocation(jf.getX(), 0);
		}
		
		int xPoint = jf.getX()+jf.getWidth();
		int h = (int) getPreferredSize().getHeight();
		int w = (int) getPreferredSize().getWidth();
		if(xPoint >= w-50) {
			this.setPreferredSize(new Dimension(w+jf.getWidth()+50, h));
		}

		w = (int) getPreferredSize().getWidth();
		h = (int) getPreferredSize().getHeight();
		int yPoint = jf.getY()+jf.getHeight();
		if(yPoint >= h-50) {
			this.setPreferredSize(new Dimension(w, h+jf.getHeight()+50));
		}
		
		return true;
	}
	
	
	
	public boolean setBulkMovement(int x, int y, JInternalFrame... objects) {

		boolean notBlocked = true;
		for(JInternalFrame w : objects) {
			if(w.getX()+x < 0) {
				x = -w.getX();
				notBlocked = false;
			}
			
			if(w.getY()+y < 0) {
				y = -w.getY();
				notBlocked = false;
			}
		}
		
		
		for(JInternalFrame w : objects) {
			w.setLocation(w.getX()+x, w.getY()+y);
			checkSize(w);
		}
		
		return notBlocked;
	}

	public List<NoteInternalFrame> getNoteByWord(Word word) {
		List<NoteInternalFrame> notes = new ArrayList<NoteInternalFrame>();
		String name = word.getNome();
		String[] synoni = word.getSynonymous();
		
		if(!name.equalsIgnoreCase(
							NoteInternalFrame.DEFAULT_TITLE)) {
			for(NoteInternalFrame note : getNotes()) {
				if(note.getTitle().trim().equalsIgnoreCase(name)) {
					notes.add(note);
				}else {
					for(String noteS : synoni) {
						if(note.getTitle().equalsIgnoreCase(noteS))
							notes.add(note);
					}
				}
			}
		}
		
		return notes;
	}
	
	public NoteInternalFrame[] getNotes() {
		List<NoteInternalFrame> wg = new ArrayList<NoteInternalFrame>();
		for(JInternalFrame ji : getAllFrames()) {
			if(ji instanceof NoteInternalFrame)
				wg.add((NoteInternalFrame) ji);
		}
		
		return wg.toArray(new NoteInternalFrame[wg.size()]);
	}
	
	public boolean existsWordGraphic(Word w) {
		return getWordGraphicsByWord(w) != null;
	}
	
	public WordGraphic getWordGraphicsByWord(Word w) {
		for(WordGraphic wb : getWordGraphics()) {
			if(wb.getWord() == w)
				return wb;
		}
		return null;
	}
	
	public Mind getMind() {
		return mind;
	}

	public void setMind(Mind mind) {
		this.mind = mind;
	}

	public synchronized WordGraphic[] getWordGraphics() {
		List<WordGraphic> wg = new ArrayList<WordGraphic>();
		for(JInternalFrame ji : getAllFrames()) {
			if(ji instanceof WordGraphic)
				wg.add((WordGraphic) ji);
		}
		
		return wg.toArray(new WordGraphic[wg.size()]);
	}
	
	public List<WordGraphic> getAllAbove(WordGraphic wg) {
		List<WordGraphic> wgList = new ArrayList<>();
		Word w =  wg.getWord();
		if(w.getSelectedMean() instanceof AbstractDefinitionMean) {
			AbstractDefinitionMean p = (AbstractDefinitionMean) w.getSelectedMean();
			for(Word wc : p.getWordList()) {
				WordGraphic wgC = getWordGraphicsByWord(wc);
				wgList.add(wgC);
				wgList.addAll(getAllAbove(wgC));
			}
		}
		
		return wgList;
	}

	public void scrollToWord(Word w) {
		scrollToFrame(getWordGraphicsByWord(w));
	}
	
	public void scrollToFrame(HierarchyInternalFrame wg) {
		Home h = IAWordBookEditor.HOME_PANEL;
		DesktopScrollPane ds = h.getDesktopScrollPane();
		
		double paneWidth = ds.getVisibleRect().getWidth();
		double paneHeight = ds.getVisibleRect().getHeight();
		JScrollBar jbH = ds.getHorizontalScrollBar();
		JScrollBar jbV = ds.getVerticalScrollBar();
		
		if(wg.getX()+wg.getWidth() > paneWidth+jbH.getValue() || 
				wg.getX()+wg.getWidth() < jbH.getValue() ||
				wg.getX() < jbH.getValue())
			ds.getHorizontalScrollBar().setValue(wg.getX());
		
		if(wg.getY()+wg.getHeight() > paneHeight+jbV.getValue() ||
				wg.getY()+wg.getHeight() < jbV.getValue() ||
				wg.getY() < jbV.getValue())
			ds.getVerticalScrollBar().setValue(wg.getY());
	}
	
	public Point getLastPopupPosition() {
		return lastPopupPosition;
	}

	public void setLastPopupPosition(Point lastPopupPosition) {
		this.lastPopupPosition = lastPopupPosition;
	}

	int lastMouseX = Integer.MIN_VALUE;
	int lastMouseY = Integer.MIN_VALUE;
	int lastXDirection = Integer.MIN_VALUE;
	int lastYDirection = Integer.MIN_VALUE;
	
	@Override public void mouseDragged(MouseEvent e) {
		
		if(lastMouseX == Integer.MIN_VALUE && 
				lastMouseY == Integer.MIN_VALUE) {
			lastMouseX = e.getX();
			lastMouseY = e.getY();
		}
		
		JInternalFrame[] components = getAllFrames();

		int xDirection = e.getX()-lastMouseX;
		int yDirection = e.getY()-lastMouseY;	
		
		// Repara alguns problemas no mouse de alguns computadores de terem pequenas voltadas no movimento da tela
		if((xDirection < 0 && lastXDirection > 0) || (xDirection > 0 && lastXDirection < 0))
			xDirection = 0;
		
		if((yDirection < 0 && lastYDirection > 0) || (yDirection > 0 && lastYDirection < 0))
			yDirection = 0;	
			
		if(!setBulkMovement(xDirection, yDirection, components)) {
			DesktopScrollPane ds = 
					(DesktopScrollPane) getParent().getParent();
			JScrollBar jsH = ds.getHorizontalScrollBar();
			JScrollBar jsV = ds.getVerticalScrollBar();
			
			for(JInternalFrame w : components) {
				if(w.getY()+yDirection < 0) {
					//yDirection = yDirection < -15 ? -15 : yDirection;
					jsV.setValue(jsV.getValue()-yDirection);
					break;
				}

			}
			for(JInternalFrame w : components) {
				if(w.getX()+xDirection < 0) {
					//xDirection = xDirection < -50 ? -50 : xDirection;
					jsH.setValue(jsH.getValue()-xDirection);
					break;
				}
			}
		}

		lastXDirection = xDirection;
		lastYDirection = yDirection;
		lastMouseX = e.getX();
		lastMouseY = e.getY();
		
	}	
	
	@Override public void mouseReleased(MouseEvent e) {

		lastMouseX = Integer.MIN_VALUE;
		lastMouseY = Integer.MIN_VALUE;
		
	}
	 
	@Override public void mousePressed(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON3) {
			lastPopupPosition = e.getPoint();
			createWordPopup.show(this, e.getX(), e.getY());
		}
	}
	
	@Override public void mouseMoved(MouseEvent e) {}
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}

	public boolean isNinbus() {
		return isNinbus;
	}

	public void setNinbus(boolean isNinbus) {
		this.isNinbus = isNinbus;
	}
	
}
