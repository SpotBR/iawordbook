package br.com.spotdev.iawordbook.test.form.wordcomponents;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserPreferences;
import com.teamdev.jxbrowser.chromium.BrowserType;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

import br.com.spotdev.iawordbook.mind.mean.onlinedefinition.AbstractOnlineDefinition;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.WhiteTitledPane;

public class OnlineDefinitionFrame 
	extends JFrame implements WindowListener {

	private static final long serialVersionUID = 151254715777596899L;
	
	public static OnlineDefinitionFrame ONLINE_DEFINITION_FRAME = new OnlineDefinitionFrame();
	List<Browser> browsers = new ArrayList<Browser>();
	
	public OnlineDefinitionFrame() {
	
		
		int typeLenght = AbstractOnlineDefinition.getClasses().length;
		WhiteTitledPane wtp = new WhiteTitledPane("Online Word Definitions");
		wtp.setLayout(new GridLayout(1, typeLenght));
		this.add(wtp);
      
        for(int i = 0; i < typeLenght; i++) {
        	BrowserPreferences.setUserAgent("Mozilla/5.0 "
        			+ "(Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B)"
        			+ " AppleWebKit/535.19 (KHTML, like Gecko)"
        			+ " Chrome/18.0.1025.133 Mobile Safari/535.19");
        	Browser browser = new Browser(BrowserType.LIGHTWEIGHT);
            BrowserView view = new BrowserView(browser);
    		wtp.add(view, BorderLayout.CENTER);
    		browsers.add(browser);
    		
        }
   
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setAlwaysOnTop(true);
	}
	
	public static void loadDefinitions(List<AbstractOnlineDefinition> aod) {
		for(int i = 0; i < aod.size(); i++) {
			Browser b = ONLINE_DEFINITION_FRAME.getBrowsers().get(i);
			b.addLoadListener(new LoadAdapter() {
	            @Override
	            public void onFinishLoadingFrame(FinishLoadingEvent event) {
	                if (event.isMainFrame()) {
	                    event.getBrowser().executeJavaScript(
	                            "document.getElementById('content').scrollIntoView();");
	                }
	            }
	        });
			b.loadURL(aod.get(i).getURL()+"#content");
			
			
		}
		ONLINE_DEFINITION_FRAME.setTitle("Online Word Definition - "+aod.get(0).getWord());
		ONLINE_DEFINITION_FRAME.setLocationRelativeTo(IAWordBookEditor.HOME_PANEL);
		ONLINE_DEFINITION_FRAME.setVisible(true);
		ONLINE_DEFINITION_FRAME.addWindowListener(ONLINE_DEFINITION_FRAME);
		ONLINE_DEFINITION_FRAME.setSize(1300,300);
		
	}

	@Override public void windowOpened(WindowEvent e) {
//		for(JScrollPane scrollPane : scrollPanes)
//			scrollPane.getVerticalScrollBar().setValue(0);
	}

	@Override public void windowActivated(WindowEvent e) {}
	@Override public void windowClosed(WindowEvent e) {}
	@Override public void windowClosing(WindowEvent e) {}
	@Override public void windowDeactivated(WindowEvent e) {}
	@Override public void windowDeiconified(WindowEvent e) {}
	@Override public void windowIconified(WindowEvent e) {}

	public List<Browser> getBrowsers() {
		return browsers;
	}

	public void setBrowsers(List<Browser> browsers) {
		this.browsers = browsers;
	}
	
	
	
}
