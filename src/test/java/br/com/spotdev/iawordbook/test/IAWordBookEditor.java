package br.com.spotdev.iawordbook.test;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.database.BackupRule;
import br.com.spotdev.iawordbook.database.WordLoader;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordAlreadyExistsException;
import br.com.spotdev.iawordbook.test.form.CheckifIsOpenedSocket;
import br.com.spotdev.iawordbook.test.form.Home;
import br.com.spotdev.iawordbook.test.form.wordcomponents.OnlineDefinitionFrame;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.NoteInternalFrame;

public class IAWordBookEditor {
	
	public static JFrame MAIN_FRAME = null;
	public static Home HOME_PANEL = null;
	
	/*
	 	P R I O R I D A D E
	 	TODO Fazer E, MAIS, +, de aparecerem diferente
	 	TODO Fazer com que gere as pastas
	 	TODO Melhorar organização do código do WordEditPanel
	 	TODO Melhorar a validação para impedir CTRL+C
	 	
	 	O U T R O S
	 	TODO Fazer outro tema em que o programa tem cor preta
	 	TODO Criar agorítimo para oganizar itens automaticamente	
	 */
	
	public static void main(String[] args) 
			throws FileNotFoundException, 
			InstantiationException, 
			IllegalAccessException, 
			IOException, WordAlreadyExistsException {
		// Check if is already openeed
		if(CheckifIsOpenedSocket.isAlreadyOpenned()) {
			JOptionPane.showMessageDialog(null, 
					"Mind WordBook is already opened in another window.", 
					"Oops", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
		
		// Load Words
		Mind mind = new Mind();
		WordLoader.readWords(mind);
		
		// Load UI Manager
//		try {
//			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
//				| UnsupportedLookAndFeelException e) {
//			e.printStackTrace();
//		}
		
		// Load View
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getScreenDevices()[0];
		GraphicsConfiguration gc = gs.getConfigurations()[0];
		MAIN_FRAME = new JFrame("Mind Word Book - by Davi Salles");
		MAIN_FRAME.setSize(gc.getBounds().getSize().width, gc.getBounds().getSize().height-20);
		MAIN_FRAME.setMinimumSize(new Dimension(850, 600));
		MAIN_FRAME.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		HOME_PANEL = new Home(mind);
		MAIN_FRAME.add(HOME_PANEL);
		MAIN_FRAME.setLocationRelativeTo(null);
		MAIN_FRAME.setVisible(true);
//		MAIN_FRAME.setAlwaysOnTop(true);
		CheckifIsOpenedSocket.setFrameToCloseServer(MAIN_FRAME);
		
		// Set de dafault selected word to the front-end
		if(mind.getWords().length > 0){
			Word firstWord = mind.getWords()[0];
			HOME_PANEL.setSelectedWord(firstWord);
			HOME_PANEL.getHierarchyPanel().scrollToWord(firstWord);
		}else {
			HOME_PANEL.getWordEditPanel().setSelected(null);
		}
		
		BackupRule.OTHER_FILES.add(NoteInternalFrame.NOTE_FILE);
		WordLoader.makeBackup(BackupRule.OTHER_FILES);
		
		new Thread(()-> {
			while(true) {

//				if(!isUserWorking()) {
//					MAIN_FRAME.setExtendedState(JFrame.MAXIMIZED_BOTH);
//					MAIN_FRAME.requestFocusInWindow();
//					MAIN_FRAME.requestFocus();
//					MAIN_FRAME.toFront();
//				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
		
	}
	
	public static void showOnlineDefinitionFrame(String word) {
		Word w = new Word(word);
		showOnlineDefinitionFrame(w);
	}
	
	public static void showOnlineDefinitionFrame(Word w) {
		if(w.getOnlineDefinitions().size() == 0) {
			try {
				w.downloadDefinitions();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, 
						"Cant download online "
						+ "word definitions\n\n"+e.getMessage(), 
						"Online Word Definitions", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		
		OnlineDefinitionFrame.loadDefinitions(w.getOnlineDefinitions());
	}
	
	public static boolean isUserWorking() {
		return MAIN_FRAME.getExtendedState() != Frame.ICONIFIED;
	}

}
