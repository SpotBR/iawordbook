package br.com.spotdev.iawordbook.test.form.wordcomponents.types;

import java.util.ArrayList;
import java.util.List;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.types.PhraseMean;
import br.com.spotdev.iawordbook.test.form.wordcomponents.AbstractDefinitionWordGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordBlock;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import java.awt.FlowLayout;

public class PhraseGraphic extends AbstractDefinitionWordGraphic {

	
	private static final long serialVersionUID = -7137638155715150013L;
	
	List<WordBlock> meanBlocks = new ArrayList<>();
	
	public PhraseGraphic(Word word) {
		super(word);
		
		JPanel meanPanel = new JPanel();
		meanPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
		getContentPane().add(meanPanel, BorderLayout.SOUTH);
		FlowLayout fl = (FlowLayout) meanPanel.getLayout();
		fl.setHgap(0);
		fl.setVgap(0);
		
		PhraseMean pm = (PhraseMean) word.getSelectedMean();
		for(Word w : pm.getWordList()) {
			addWordBlock(w, meanPanel);
		}
		
		this.pack();
	}
	
	@Override
	public WordBlock addWordBlock(Word w, JPanel p) {
		JLabel wordLabel_1 = new JLabel(w.getNome());
		wordLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		WordBlock jp = new WordBlock(w,wordLabel_1);
		jp.setBorder(new BevelBorder(BevelBorder.LOWERED));
		jp.add(wordLabel_1);

		p.add(jp);
		meanBlocks.add(jp);
		
		return jp;
	}
	
	@Override
	public List<WordBlock> getMeanBlocks() {
		return meanBlocks;
	}

	@Override
	public String getTitleTag() {
		return "PH";
	}

	@Override
	public Border getBorderStyle() {
		return new BevelBorder(BevelBorder.RAISED,Color.GRAY,Color.GRAY);
	}
}
