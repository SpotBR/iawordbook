package br.com.spotdev.iawordbook.test.form.wordcomponents.types;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Utilities;

import br.com.spotdev.iawordbook.database.WordLoader;
import br.com.spotdev.iawordbook.test.form.UndoTool;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyInternalFrame;

public class NoteInternalFrame extends HierarchyInternalFrame 
	implements DocumentListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5832502909797096596L;
	public static final File NOTE_FILE = new File("notes.properties");
	public static final String DEFAULT_TITLE = "NOTE";
	private static final String QUESTION_PATTERN = "([^\n]*)\\?";
	
	public static long LAST_NOTING_DELAY = System.currentTimeMillis();
	public static final long ATTENTION_CHECK_DELAY = 120000;
	public static long FIRST_ATTENTION_TIME = System.currentTimeMillis();
	static {
		startNoteAttentionSystem();
	}
	
	private String id = null;
	private JTextArea jtext = null;
	private String title = null;
	private List<String> questions = new ArrayList<String>();
	
	public List<String> getQuestions() {
		return questions;
	}

	public void setQuestions(List<String> questions) {
		this.questions = questions;
	}

	private boolean hasChangeToSave = false;

	public NoteInternalFrame() {
		this.setSize(150, 100);
		this.setResizable(true);
		this.setBorder(new EmptyBorder(1, 1, 1, 1));
		
		jtext = new AskTextArea();
		this.setBackground(new Color(255, 233, 141));
		
		jtext.addMouseMotionListener(getMouseEvent());
		jtext.addMouseListener(getMouseEvent());
		
		this.setLayout(new BorderLayout());
		this.add(jtext, BorderLayout.CENTER);
		
		JMenuItem renameMenuItem = new JMenuItem("* Change title");
		renameMenuItem.addActionListener(this::pressChangeTitleEvent);
		getPopUpMenu().add(renameMenuItem);
		
		jtext.getDocument().addDocumentListener(this);
		new Thread(this::hasChangeEvent).start();

	}
	
	public static void resetAttentionSystem() {
		if(FIRST_ATTENTION_TIME == 0)
			FIRST_ATTENTION_TIME = System.currentTimeMillis();
		LAST_NOTING_DELAY = System.currentTimeMillis();
	}
	
	public class AskTextArea extends JTextArea {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7777446349613811970L;
		
		public AskTextArea() {
			this.setFont(new Font("Helvetica", Font.BOLD, 12));
			this.setBorder(new EmptyBorder(10, 10, 0, 10));
			this.setWrapStyleWord(true);
			this.setLineWrap(true);
			this.setOpaque(false);
		}
		
		@Override
		protected void paintComponent(Graphics g) {

			for(Rectangle r : getQuestionsRectangles()) {
				g.setColor(new Color(235, 183, 52));
//				System.out.println((int)r.getX()+" - "+(int)r.getY()+" - "+(int)r.getWidth()+" - "+(int)r.getHeight());
				g.fillRect((int)r.getX(), (int)r.getY(), (int)r.getWidth(), (int)r.getHeight());
			}

			super.paintComponent(g);
		}
	}
	
	@Override
	public String getTitleTag() {
		return title == null ? DEFAULT_TITLE : title;
	}

	private static String e(String s) {
		return Base64.getEncoder().encodeToString(s.getBytes());
	}
	
	private static String d(String s) {
		return new String(Base64.getDecoder().decode(s.getBytes()));
	}
	
	private static String e(int i) {
		return e(i+"");
	}
	
	private void pressChangeTitleEvent(ActionEvent e) {
		String message = JOptionPane.showInputDialog(null,
				"Insert the new title name",getTitle());
		
		if(message != null && !message.isEmpty()) {
			this.setTitle(message);
			title = message;
			save();
		}
	}
	
	@Override public boolean save() {
		
		
		try {
			String data = 
					e(getX())+" "+e(getY())+" "+e(getWidth())+
					" "+e(getHeight())+" "+e(getJtext().getText())+" "+e(getTitle());
			
			NOTE_FILE.createNewFile();
			
			synchronized (WordLoader.FILE_THREAD_LOCK) {
				Properties properties = new Properties();
				FileInputStream fis = new FileInputStream(NOTE_FILE);
				InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
				properties.load(isr);
				isr.close();
				
				properties.setProperty(getId(), data);
				FileOutputStream fos = new FileOutputStream(NOTE_FILE);
				properties.store(fos, "For my creator");
				fos.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}
	
	public static NoteInternalFrame[] loadAll() {
		List<NoteInternalFrame> notes = new ArrayList<>();
		try {
			NOTE_FILE.createNewFile();
			synchronized (WordLoader.FILE_THREAD_LOCK) {
				Properties properties = new Properties();
				FileInputStream fis = new FileInputStream(NOTE_FILE);
				InputStreamReader isr = new InputStreamReader(fis, "UTF-8");

				properties.load(isr);
				isr.close();
				
				for(Object key : properties.keySet()) {
					String id = (String)key;
					NoteInternalFrame note = new NoteInternalFrame();
					String data = properties.getProperty((String)key);
					String values[] = data.split(" ");
					int x = Integer.parseInt(d(values[0]));
					int y = Integer.parseInt(d(values[1]));
					int w = Integer.parseInt(d(values[2]));
					int h = Integer.parseInt(d(values[3]));
					String text = d(values[4]);
					String title = null;
					if(values.length > 5) {
						title = d(values[5]);
					}else {
						title = "NOTE";
					}
					
					note.setBounds(x, y, w, h);
					note.getJtext().setText(text);
					note.setTitle(title);
					note.setId(id);
					note.refreshQuestionsString();
					notes.add(note);
					
					UndoTool.addUndoFunctionality(note.getJtext());
				}
			}
				
				
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return notes.toArray(new NoteInternalFrame[notes.size()]);
	}
	
	public JTextArea getJtext() {
		return jtext;
	}

	public String getId() {
		if(id == null)
			generateId();
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	private String generateId() {
		id = UUID.randomUUID().toString(); 
		return id;
	}

	@Override
	public void onPressOnObject(MouseEvent e) {
		
	}

	private void hasChangeEvent() {
		while(true) {
			if(hasChangeToSave) {
				save();
				hasChangeToSave = false;
			}
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {e.printStackTrace();}
		}
	}
	
	@Override public void changedUpdate(DocumentEvent e) {
		repaint();
		refreshQuestionsString();
		hasChangeToSave = true;			
		resetAttentionSystem();
	}
	
	@Override public void insertUpdate(DocumentEvent e) {
		repaint();					
		resetAttentionSystem();
		hasChangeToSave = true;
	}
	@Override public void removeUpdate(DocumentEvent e) {
		repaint();					
		resetAttentionSystem();
		hasChangeToSave = true;
	}

	@Override public void removeSavedData() throws Exception {
		synchronized (WordLoader.FILE_THREAD_LOCK) {
			Properties properties = new Properties();
			FileInputStream fis = new FileInputStream(NOTE_FILE);
			InputStreamReader isr = new InputStreamReader(fis, "UTF-8");

			properties.load(isr);
			isr.close();
			
			properties.remove(getId());
			FileOutputStream fos = new FileOutputStream(NOTE_FILE);
			properties.store(fos, "For my creator");
			fos.close();
			
		}
	}
	
	private void refreshQuestionsString(){
	    Pattern pattern = Pattern.compile(QUESTION_PATTERN);
	    Matcher matcher = pattern.matcher(jtext.getText());
	    questions.clear();

	    while(matcher.find()) {
	    	if(!matcher.group().startsWith("R:"))
	    		questions.add(matcher.group());
	    }
	    
	}
 	
	private List<Rectangle> getQuestionsRectangles(){
		
	    Pattern pattern = Pattern.compile(QUESTION_PATTERN);
	    Matcher matcher = pattern.matcher(jtext.getText());
	    List<Rectangle> retangulo = new ArrayList<Rectangle>();
	    
	    while(matcher.find())
	    {
	    	if(matcher.group().startsWith("R:"))
	    		continue;
	    	
	    	int line = -1;
	    	int lastLine = -1;
	    	try {
				line = getLineFromOffset(matcher.start());
				lastLine = line+getLineCount(matcher);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
	        int lineHeight = 15;
	        int margin = 10;
	        retangulo.add(new Rectangle(0,margin+(line*lineHeight),getWidth(),lineHeight*(lastLine-line+1)));
	    }
		return retangulo;
	}
	
	public int getLineFromOffset(int offset) throws BadLocationException {
		String txt = jtext.getText();
		int lastEnd = 0;
		int totalChars = 0;
		int line = 0;
		while(totalChars != txt.length()) {
			lastEnd = Utilities.getRowEnd(jtext, lastEnd);
			if(lastEnd >= offset) {
				return line;
			}
			
			totalChars+=lastEnd;
			line ++;
			lastEnd+=1;
		}
		return -1;

	}
	
	public int getLineCount(Matcher matcher) throws BadLocationException {

		String txt = matcher.group();
		List<String> lines = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < txt.toCharArray().length; i++) {
			int offset = i+Utilities.getRowStart(jtext, matcher.start());
			char c = txt.toCharArray()[i];
		    sb.append(c);
		    
		    if(offset == Utilities.getRowEnd(jtext, offset)) { 
		        lines.add(sb.toString());
//		        System.out.println(sb);
		        sb = new StringBuilder(); 
		    }
		}
		
		return lines.size();
	}
	
	public static synchronized void beep() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
			   Clip clip = null;

			    try {
			        AudioInputStream inputStream = 
			        		AudioSystem.getAudioInputStream(
			        				new File("beep.wav"));
			        DataLine.Info info = 
			        		new DataLine.Info(Clip.class, 
			        				inputStream.getFormat());
			        clip = (Clip)AudioSystem.getLine(info);
			        clip.open(inputStream);
			        clip.start();
			    } catch (Exception ex) {
			        ex.printStackTrace();
			    }

			}
		});
		
		t.start();
	}
	
	public static void startNoteAttentionSystem() {
		new Thread(new NoteAttentionRunnable()).start();
	}

	private static class NoteAttentionRunnable
		implements Runnable {

		@Override
		public void run() {
			while(true) {
				if((System.currentTimeMillis()-
						LAST_NOTING_DELAY 
						>= ATTENTION_CHECK_DELAY && 
						LAST_NOTING_DELAY != 0)) {
					FIRST_ATTENTION_TIME=FIRST_ATTENTION_TIME+ATTENTION_CHECK_DELAY;
					String time = String.format("%02d min, %02d seg", 
						    TimeUnit.MILLISECONDS.toMinutes(
						    		System.currentTimeMillis()
						    		-FIRST_ATTENTION_TIME),
						    TimeUnit.MILLISECONDS.toSeconds(
						    		System.currentTimeMillis()
						    		-FIRST_ATTENTION_TIME) - 
						    TimeUnit.MINUTES.toSeconds(
						    		TimeUnit.MILLISECONDS.toMinutes(
						    				System.currentTimeMillis()
						    				-FIRST_ATTENTION_TIME))
						);
					LAST_NOTING_DELAY = 0;
					FIRST_ATTENTION_TIME = 0;
					beep();
					JOptionPane.showMessageDialog(null, 
							"Você conseguiu prestar atenção por "+time);
				}
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
}
