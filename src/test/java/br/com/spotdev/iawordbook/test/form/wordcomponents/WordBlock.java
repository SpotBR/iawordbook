package br.com.spotdev.iawordbook.test.form.wordcomponents;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.AbstractDefinitionMean;
import br.com.spotdev.iawordbook.mind.mean.debugger.Incomplete;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyPanel;

public class WordBlock extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -775433083326434916L;
	public static Color DEFAULT_COLOR = null;

	Word word = null;
	JLabel wLabel = null;
	public WordBlock(Word word, JLabel wLabel) {
		DEFAULT_COLOR = this.getBackground();
		this.word = word;
		this.wLabel = wLabel;
		wLabel.setFont(new Font("Helvetica", 
				Font.BOLD, WordGraphic.WORD_FONT_SIZE));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		HierarchyPanel h = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
		Word parent[] = h.getMind().getPattern(getWord());
		if(parent.length != 0 && 
				parent[0].getSelectedMean() instanceof AbstractDefinitionMean) {
			AbstractDefinitionMean ph = 
					(AbstractDefinitionMean) parent[0].getSelectedMean();
			if(!ph.isMeanComplete() && isOnIncompleteList(ph.getIncompletes()))
				this.setBackground(new Color(255, 153, 153));
			else
				this.setBackground(DEFAULT_COLOR);
		}
	}
	
	private boolean isOnIncompleteList(Incomplete[] incomplete) {
		for(Incomplete in : incomplete) {
			if(in.getWord() == this.getWord())
				return true;
		}
		
		return false;
	}
	
	public Word getWord() {
		return word;
	}
	public void setWord(Word word) {
		this.word = word;
	}
	public JLabel getwLabel() {
		return wLabel;
	}
	public void setwLabel(JLabel wLabel) {
		this.wLabel = wLabel;
	}
	
}
