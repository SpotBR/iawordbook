package br.com.spotdev.iawordbook.test.form;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordHasPatternDependence;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.hierarchy.DesktopScrollPane;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyInternalFrame;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyPanel;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordGraphic;
import br.com.spotdev.iawordbook.test.form.wordedit.WordEditPanel;

public class Home extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HierarchyPanel hierarchyPanel = null;
	private Word selectedWord = null;
	private WordEditPanel wordEditPanel = null;
	private DesktopScrollPane desktopScrollPane = null;
	private JCheckBoxMenuItem styleNimbusItem = null;
	private JCheckBoxMenuItem styleJavaDefaultItem = null;
	private JCheckBoxMenuItem styleOSDefaultItem = null;

	@SuppressWarnings("deprecation")
	public Home(Mind mind) {
		this.setLayout(new BorderLayout());
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menuEdit = new JMenu("Edit");
		JMenuItem clearItem = 
				new JMenuItem("Clear");
		clearItem.addActionListener(this::onPressClearItem);
		JMenuItem searchItem = 
				new JMenuItem("Search Word");
		searchItem.addActionListener(this::onPressSearchItem);
		searchItem.setAccelerator(KeyStroke.getKeyStroke('F', 
				Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		JMenuItem checkOnlineItem = 
				new JMenuItem("Check Word Online");
		checkOnlineItem.addActionListener(this::onPressCheckOnlineItem);
		checkOnlineItem.setAccelerator(KeyStroke.getKeyStroke('D', 
				Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		JMenuItem quitItem = 
				new JMenuItem("Quit");
		quitItem.addActionListener((a)->System.exit(0));
		
		menuBar.add(menuEdit);
		menuEdit.add(clearItem);
		menuEdit.add(searchItem);
		menuEdit.add(checkOnlineItem);
		menuEdit.addSeparator();
		menuEdit.add(quitItem);
		
		JMenu menuStyle = new JMenu("Style");
		styleNimbusItem = 
				new JCheckBoxMenuItem("Nimbus",false);
		styleNimbusItem.addActionListener(this::onPressChangeStyleItem);
		
		styleJavaDefaultItem = 
				new JCheckBoxMenuItem("Java Default (Fatest)", false);
		styleJavaDefaultItem.addActionListener(this::onPressChangeStyleItem);
		
		styleOSDefaultItem = 
				new JCheckBoxMenuItem("OS Style", true);
		styleOSDefaultItem.addActionListener(this::onPressChangeStyleItem);
		
		menuBar.add(menuStyle);
		menuStyle.add(styleOSDefaultItem);
		menuStyle.add(styleNimbusItem);
		menuStyle.add(styleJavaDefaultItem);
		
		JMenu menuView = new JMenu("View");
		JCheckBoxMenuItem viewHierarchyItem = 
				new JCheckBoxMenuItem("Hierarchy Editor", false);
		viewHierarchyItem.setEnabled(false);
		
		JCheckBoxMenuItem viewWordEditItem = 
				new JCheckBoxMenuItem("Word Edit", true);
		viewWordEditItem.addActionListener((a)->
				getWordEditPanel().setVisible(!getWordEditPanel().isVisible()));
		viewWordEditItem.setAccelerator(KeyStroke.getKeyStroke('1', 
				Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuBar.add(menuView);
		menuView.add(viewHierarchyItem);
		menuView.add(viewWordEditItem);
		
		IAWordBookEditor.MAIN_FRAME.setJMenuBar(menuBar);
		
		JPanel paddingPanel = new JPanel();
		paddingPanel.setBackground(Color.WHITE);
		paddingPanel.setBorder(new EmptyBorder(10,10,10,10));
		paddingPanel.setLayout(new BorderLayout());
		
		wordEditPanel = new WordEditPanel(mind);
		hierarchyPanel = new HierarchyPanel(mind);
		desktopScrollPane = new DesktopScrollPane(hierarchyPanel);		
		desktopScrollPane.setBorder(new TitledBorder("Hierarchy"));
		desktopScrollPane.setBackground(Color.WHITE);
		
		paddingPanel.add(wordEditPanel, BorderLayout.WEST);
		paddingPanel.add(desktopScrollPane, BorderLayout.CENTER);
		this.add(paddingPanel);
		
		hierarchyPanel.refreshSize();
	}

	public void onPressClearItem(ActionEvent ae) {
		List<HierarchyInternalFrame> tryAfter = new ArrayList<HierarchyInternalFrame>();
		boolean notTried = true;
		while(notTried || !tryAfter.isEmpty()){
			notTried = false;
			for(JInternalFrame ji : getHierarchyPanel().getAllFrames()){
				if(ji instanceof HierarchyInternalFrame){
					HierarchyInternalFrame hi = (HierarchyInternalFrame) ji; 
					try {
						getHierarchyPanel().removeWordAndGraphic(hi);
						tryAfter.remove(hi);
					} catch (WordHasPatternDependence e) {
						tryAfter.add(hi);
					} catch (IOException e) {
						tryAfter.add(hi);
						e.printStackTrace();
					}
				}
			}
		}
		getDesktopScrollPane().resizeDesktop();
	}
	
	public void onPressSearchItem(ActionEvent ae) {
		getWordEditPanel().setVisible(true);
		getWordEditPanel().getSearchField().requestFocus();
	}
	
	public void onPressCheckOnlineItem(ActionEvent ae) {
		String word = null;
		while(true) {
			String defaultText = "";
			try {
				defaultText = (String) Toolkit.getDefaultToolkit().
						getSystemClipboard().getData(DataFlavor.stringFlavor);
			} catch (HeadlessException | UnsupportedFlavorException | IOException e1) {
				e1.printStackTrace();
			}
			
			word = (String) JOptionPane.showInputDialog(this, 
					"Inserte the word name that you want to download definition:", 
					"Generate Word Definition",
					JOptionPane.PLAIN_MESSAGE, null, null, defaultText);
			
			if(word == null)
				break;

			word = word.trim();
			if(!word.matches("[a-zA-ZÀ-ú\\s]+")) {
				JOptionPane.showMessageDialog(this, 
						"The word can compose just letters without spaces.", 
						"Generate Word Definition", JOptionPane.ERROR_MESSAGE);
				continue;
			}
			
			break;
		}
		
		if(word != null) {
			IAWordBookEditor.showOnlineDefinitionFrame(word);
		}
	}
	
	public void onPressChangeStyleItem(ActionEvent ae) {
		JCheckBoxMenuItem item = (JCheckBoxMenuItem) ae.getSource();
		
		String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
		getHierarchyPanel().setNinbus(item == styleNimbusItem);
		if(item == styleJavaDefaultItem) {
			lookAndFeel = "javax.swing.plaf.metal.MetalLookAndFeel";
			styleJavaDefaultItem.setSelected(true);
			styleNimbusItem.setSelected(false);
			styleOSDefaultItem.setSelected(false);
		}else if(item == styleNimbusItem) {
			lookAndFeel = "javax.swing.plaf.nimbus.NimbusLookAndFeel";
			styleJavaDefaultItem.setSelected(false);
			styleNimbusItem.setSelected(true);
			styleOSDefaultItem.setSelected(false);
		}else if(item == styleOSDefaultItem){
			lookAndFeel = UIManager.getSystemLookAndFeelClassName();
			styleJavaDefaultItem.setSelected(false);
			styleNimbusItem.setSelected(false);
			styleOSDefaultItem.setSelected(true);
		}
		IAWordBookEditor.MAIN_FRAME.setVisible(false);
		try {
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		SwingUtilities.updateComponentTreeUI(IAWordBookEditor.MAIN_FRAME);
		IAWordBookEditor.MAIN_FRAME.setVisible(true);
	}
	
	public HierarchyPanel getHierarchyPanel() {
		return hierarchyPanel;
	}

	public Word getSelectedWord() {
		return selectedWord;
	}

	public WordEditPanel getWordEditPanel() {
		return wordEditPanel;
	}

	public void setWordEditPanel(WordEditPanel wordEditPanel) {
		this.wordEditPanel = wordEditPanel;
	}
	
	public void clearSelected() {
		wordEditPanel.setSelected(null);
	}

	public void setSelectedWord(Word selectedWord) {
		HierarchyPanel hi = getHierarchyPanel();
		WordGraphic wgBefore = hi.getWordGraphicsByWord(this.selectedWord);
		
		this.selectedWord = selectedWord;
		
		WordEditPanel wordEditPanel = getWordEditPanel();
		WordGraphic wg = hi.getWordGraphicsByWord(selectedWord);
		hi.setSelectedFrame(wg);
		wordEditPanel.setSelected(wg);
		
		if(wgBefore != null) {
			try {
				wgBefore.setSelected(true);
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
			wgBefore.repaint();
		}
		
		try {
			wg.setSelected(true);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		wg.repaint();
	}

	public DesktopScrollPane getDesktopScrollPane() {
		return desktopScrollPane;
	}

	public JCheckBoxMenuItem getStyleNimbusItem() {
		return styleNimbusItem;
	}
	
	
	
}
