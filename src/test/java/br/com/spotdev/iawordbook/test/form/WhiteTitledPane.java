package br.com.spotdev.iawordbook.test.form;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class WhiteTitledPane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4120109475429018849L;
	private TitledBorder titledBorder = null;
	
	public WhiteTitledPane(String titled) {
		this.setBackground(Color.WHITE);
		titledBorder = new TitledBorder(null, titled, 
				TitledBorder.LEADING, TitledBorder.TOP, null, null);
		this.setBorder(titledBorder);
	}
	
	public void setTitle(String title) {
		titledBorder.setTitle(title);
	}
	
}