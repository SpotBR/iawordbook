package br.com.spotdev.iawordbook.test.form.wordedit;

import java.awt.Point;
import java.awt.event.ActionEvent;import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.text.JTextComponent;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.database.BackupRule;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordAlreadyExistsException;
import br.com.spotdev.iawordbook.mind.mean.AbstractDefinitionMean;
import br.com.spotdev.iawordbook.mind.mean.MeanType;
import br.com.spotdev.iawordbook.mind.mean.debugger.Ambiguous;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.Home;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyInternalFrame;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyPanel;
import br.com.spotdev.iawordbook.test.form.wordcomponents.AbstractDefinitionWordGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordBlock;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordGraphic;
import br.com.spotdev.iawordbook.mind.mean.types.PoleMean;

public class WordEditPanel extends AbstractWordEditPanel 
	implements FocusListener, KeyListener, DocumentListener {

	private static final long serialVersionUID = -5623139940018792169L;
	private boolean waitingToSaveDescription = false;
	private boolean waitingToSaveDefinition = false;
	private boolean waitingToSaveWordName = false;
	private boolean waitingToSaveSynonymous = false;
	
	public WordEditPanel(Mind mind) {
		super(mind);
		getWordList().addListSelectionListener(this::listChangedSearchList);
		getPhraseRadio().addActionListener(this::changeWordTypeToPhrase);
		getNoneRadio().addActionListener(this::changeWordTypeToNone);
		getEmpiricRadio().addActionListener(this::changeWordTypeToEmpiric);
		getPoleRadio().addActionListener(this::changeWordTypeToPole);
		
		getDefinitionField().addFocusListener(this);
		getDescriptionField().addFocusListener(this);
		getWordNameField().addFocusListener(this);
		getWrapField().addFocusListener(this);
		getWrappedField().addFocusListener(this);
		getFieldSynonymous().addFocusListener(this);
		
		getWrapField().addKeyListener(this);
		getWrappedField().addKeyListener(this);
		getDefinitionField().addKeyListener(this);
		getDescriptionField().addKeyListener(this);
		getWordNameField().addKeyListener(this);
		getFieldSynonymous().addKeyListener(this);
		
		getSearchField().getDocument().addDocumentListener(this);

		refreshSearchCount(0);
	
	}
	
	private String replaceDuplicates(String definition) {
		String[] words = definition.split(" ");
		List<String> items = new LinkedList<>();
		for(String v : words)items.add(v);
		boolean duplicates=false;
		boolean first = true;
		main: while(duplicates || first) {
			first = false;
			for (int j=0;j<items.size();j++)
			  for (int k=j+1;k<items.size();k++) {
			    if (k!=j && items.get(k).equalsIgnoreCase(items.get(j))) {
			      duplicates=true;
			      items.remove(j);
			      continue main;
			    }
			  }
			
			duplicates = false;
		}
		
		String newDefinition = "";
		for(String v : items)newDefinition+=v+" ";
		newDefinition = newDefinition.trim();
		return newDefinition.equalsIgnoreCase(definition) ? null : newDefinition;
	}
	
	public void fieldChangeEvent(JTextComponent field) {

		WordGraphic wg = getWordGraphic();
		Word w = wg.getWord();
		List<String> refreshWordsComponents = new ArrayList<>();
		if(waitingToSaveDefinition) {
			
			field.setText(field.getText().trim());
			String definition = field.getText().toLowerCase();
			String cleanDuplicates = replaceDuplicates(definition);
				
			if(cleanDuplicates == null) {
				List<String> definitionWords = Arrays.asList(
						definition.toLowerCase().split(" "));
				
				if(definitionWords.contains(
						w.getNome().toLowerCase())) {
					field.setText(definition);
					JOptionPane.showMessageDialog(this, "The definition can not be the word itself.",
							"Word Edit", JOptionPane.ERROR_MESSAGE);
					field.requestFocus();
					return;
				}
				
				for(String word : definitionWords) {
					Ambiguous isAmbiguous = 
							getMind().isAmbiguous(word, getWordGraphic().getWord());
					
					if(isAmbiguous != null) {
						field.setText(definition);
						JOptionPane.showMessageDialog(
								this, isAmbiguous.getMessage(),
								"Word Edit",
								JOptionPane.ERROR_MESSAGE);
						field.requestFocus();
						return;
					}

					Word wordBySynonymou = getMind().getWordBySynonymous(word);
					if(wordBySynonymou != null) {
						field.setText(definition);
						JOptionPane.showMessageDialog(
								this, "\""+word.toUpperCase()+
										"\" is synonymous "
										+ "from \""+wordBySynonymou.getNome()
										.toUpperCase()+
										"\" created by you, use it to "
										+ "define \""+w.getNome().toUpperCase()+"\" please.",
								"Word Edit",
								JOptionPane.ERROR_MESSAGE);
						field.requestFocus();
						return;
					}
				}
				
				if(w.getSelectedMean() instanceof PoleMean){
					if(definition.contains(" ")){
						field.setText(definition.replaceAll(" ", " "));
						JOptionPane.showMessageDialog(this, "A pole definition can not have spaces.",
								"Word Edit", JOptionPane.ERROR_MESSAGE);
						field.requestFocus();
						return;
					}
					
					PoleMean pm = (PoleMean)w.getSelectedMean();
					String wrap = pm.getWrap() == null ? 
							"" : pm.getWrap().getNome();
					String wrapped = pm.getWrapped() == null ? 
							" " : " "+pm.getWrapped().getNome();
					if(field == getWrappedField()){
						wrapped = " "+definition;
					}else if(field == getWrapField()){
						wrap = definition;
					}
					
					definition = wrap+wrapped;
				}
				
				refreshWordsComponents.addAll(getNewWords(definition));
				w.getSelectedMean().setup(getMind(), definition);
				BackupRule.checkBackup();
				
			}else{
				field.setText(cleanDuplicates);
				JOptionPane.showMessageDialog(this, "Definition can not have duplicate words.",
						"Word Edit", JOptionPane.ERROR_MESSAGE);
				field.requestFocus();
				return;
			}
			
		}
		
		if(waitingToSaveSynonymous) {
			field.setText(field.getText().trim());
			String synonymous = field.getText().toLowerCase();
			String cleanDuplicates = replaceDuplicates(synonymous);
			
			if(synonymous.equalsIgnoreCase(w.getSynonymousToString())) {
				waitingToSaveSynonymous = false;
				field.requestFocus();
				return;
			}
			
			if(cleanDuplicates == null) {
				String synonTrim = synonymous.toLowerCase().trim();
				List<String> synonymoysWords = !synonTrim.isEmpty() ?
						Arrays.asList(synonTrim.split(" ")) : new ArrayList<>();
				
				for(String word : synonymoysWords) {
					if(getMind().existsSynonymous(w,synonymous)) {
						field.setText(synonymous);
						JOptionPane.showMessageDialog(this, "The word "+word+" already exists.", 
								"Renaming Word", JOptionPane.ERROR_MESSAGE);
						field.requestFocus();
						return;
					}
				}
				
				if(synonymoysWords.contains(
						w.getNome().toLowerCase())) {
					field.setText(synonymous);
					JOptionPane.showMessageDialog(this, "The synonymous can not be the word itself.",
							"Word Edit", JOptionPane.ERROR_MESSAGE);
					field.requestFocus();
					return;
				}else {
					w.setSynonymous(synonymoysWords.toArray(new String[synonymoysWords.size()]));
				}
			}else{
				field.setText(cleanDuplicates);
				JOptionPane.showMessageDialog(this, "Synonymous can not have duplicate words.",
						"Word Edit", JOptionPane.ERROR_MESSAGE);
				field.requestFocus();
				return;
			}
			
		}
		
		if(waitingToSaveDescription) {
			getDescriptionField().setText(
					getDescriptionField().getText().trim());
			w.setDescription(getDescriptionField().getText());
		}
		
		if(waitingToSaveWordName && 
				!getWordNameField().getText().equalsIgnoreCase(w.getNome())) {
			if(!getMind().contains(getWordNameField().getText())) {
				if(getWordNameField().getText().equals("")) {
					getWordNameField().setText(w.getNome());
					JOptionPane.showMessageDialog(this, "Definition can not be empty.",
							"Word Edit", JOptionPane.ERROR_MESSAGE);
					getDefinitionField().requestFocus();
					waitingToSaveWordName = false;
				}else {
					w.setNome(getWordNameField().getText().toLowerCase());
					w.getOnlineDefinitions().clear();
					w.downloadDefinitionsBackground(getMind());
					HierarchyPanel h = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
					List<WordGraphic> wordsToSave = new ArrayList<>();
					
					for(WordGraphic wgIndex : h.getWordGraphics()) {
						if(wgIndex instanceof AbstractDefinitionWordGraphic) {
							WordBlock wb = ((AbstractDefinitionWordGraphic) wgIndex).getBlockByWord(w);
							if(wb != null) {
								wordsToSave.add(wgIndex);
							}
						}
					}
					wordsToSave.forEach((v)->{
						save(v);
					});
					
				}
			}else {
				getWordNameField().setText(w.getNome());
				JOptionPane.showMessageDialog(this, "This word already exists.", 
						"Renaming Word", JOptionPane.ERROR_MESSAGE);
				waitingToSaveWordName = false;

			}
			
		}

		if(waitingToSaveDefinition || 
				waitingToSaveDescription || 
				waitingToSaveWordName || waitingToSaveSynonymous) {
			save(wg);
			waitingToSaveDefinition = false;
			waitingToSaveDescription = false;
			waitingToSaveWordName = false;
			waitingToSaveSynonymous = false;
			
			HierarchyPanel hi = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
			
			Point p = null;
			for(String wS : refreshWordsComponents) {
				p = hi.resetChildPosition(
						hi.getWordGraphicsByWord(getMind().getWord(wS)),
						p);
			}

			getWordList().updateUI();
		} 
		
	}
	
	public List<String> getNewWords(String words){
		String[] wordArray = words.split(" ");
		List<String> isNewList = new ArrayList<>();
		for(String s : wordArray) {
			if(!getMind().contains(s) && !s.isEmpty())
				isNewList.add(s);
		}
		
		return isNewList;
	}
	
	public WordGraphic getWordGraphic() {
		Home h = IAWordBookEditor.HOME_PANEL;
		HierarchyPanel hi = h.getHierarchyPanel();
		return hi.getWordGraphicsByWord(h.getSelectedWord());
	}
	
	public void changeWordTypeToPhrase(ActionEvent a) {
		choseRadioButton(MeanType.PHRASE);
	}
	
	public void changeWordTypeToEmpiric(ActionEvent a) {
		choseRadioButton(MeanType.EMPIRIC);
	}

	public void changeWordTypeToPole(ActionEvent a) {
		choseRadioButton(MeanType.POLE);
	}
	
	public void changeWordTypeToNone(ActionEvent a) {
		choseRadioButton(MeanType.NONE);
	}

	public void choseRadioButton(MeanType type) {
		switch(type) {
			case PHRASE:
				getEmpiricRadio().setSelected(false);
				getNoneRadio().setSelected(false);
				getPhraseRadio().setSelected(true);
				getPoleRadio().setSelected(false);
				break;
			case NONE:
				getEmpiricRadio().setSelected(false);
				getNoneRadio().setSelected(true);
				getPhraseRadio().setSelected(false);
				getPoleRadio().setSelected(false);
				getDefinitionField().setText("");
				break;
			case EMPIRIC:
				getEmpiricRadio().setSelected(true);
				getNoneRadio().setSelected(false);
				getPhraseRadio().setSelected(false);
				getPoleRadio().setSelected(false);
				break;
			case POLE:
				getEmpiricRadio().setSelected(false);
				getNoneRadio().setSelected(false);
				getPhraseRadio().setSelected(false);
				getPoleRadio().setSelected(true);
				break;
			default:
				break;
		}		
		
		WordGraphic wg = getWordGraphic();
		wg.getWord().setMeanType(type);
		setEnabled(true);

		
		// Arruma a posição dos wordgraphics apagados do type antigo que precisaram
		// ser recriados
		List<Word> refreshPosition = new ArrayList<>();
		if(wg.getWord().getSelectedMean() instanceof 
				AbstractDefinitionMean) {
			AbstractDefinitionMean abs = 
					(AbstractDefinitionMean) wg.getWord().getSelectedMean();
			if(abs.getWordList().size() != 0) {
				for(Word w : abs.getWordList()) {
					if(!getMind().contains(w.getNome())) {
						try {
							getMind().add(w);
						} catch (WordAlreadyExistsException e) {
							e.printStackTrace();
						}
						refreshPosition.add(w);
					}
				}
				
				
			}
		}
		
		save(wg);
		Point p = null;
		for(Word w : refreshPosition) {
			HierarchyPanel hi = 
					IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
			p = hi.resetChildPosition(hi.getWordGraphicsByWord(w), p);
		}
	}
	
	public void save(WordGraphic wg) {
		Home h = IAWordBookEditor.HOME_PANEL;
		HierarchyPanel hi = h.getHierarchyPanel();
		hi.refreshWordGraphic(wg);
		
	}
	
	public void listChangedSearchList(ListSelectionEvent li){
		Object o = getWordList().getSelectedValue();
		if(o != null) {
			HierarchyInternalFrame hif = 
					((WordListItemModel) 
					getWordList().getSelectedValue()).getHierarchy();
			Home h = IAWordBookEditor.HOME_PANEL;
			if(hif instanceof WordGraphic) {
				WordGraphic wg = (WordGraphic) hif;
				h.setSelectedWord(wg.getWord());
			}
			
			HierarchyPanel hi = h.getHierarchyPanel();
			hi.scrollToFrame(hif);
		}
		
		refreshSearchCount();
	}
	
	public void setSelected(WordGraphic word) {
		this.setEnabled(word != null);
		if(word == null) return;
		
		Word w = word.getWord();
		if(w.getType() != MeanType.NONE){
			String value = w.getSelectedMean().getValue();
			if(w.getType() != MeanType.POLE)
				getDefinitionField().setText(value != null ? value : "");
			else {
				PoleMean pm = (PoleMean) word.getWord().getSelectedMean();
				String wrap = pm.getWrap() != null ? pm.getWrap().getNome() : "";
				String wrapped = pm.getWrapped() != null ? pm.getWrapped().getNome() : "";
				
				getWrapField().setText(wrap);
				getWrappedField().setText(wrapped);
			}
		}
		
		switch(w.getType()){
			case PHRASE:
				getPhraseRadio().setSelected(true);
				getEmpiricRadio().setSelected(false);
				getNoneRadio().setSelected(false);
				getPoleRadio().setSelected(false);
				break;
			case EMPIRIC:
				getPhraseRadio().setSelected(false);
				getEmpiricRadio().setSelected(true);
				getNoneRadio().setSelected(false);
				getPoleRadio().setSelected(false);
				break;
			case NONE:
				getPhraseRadio().setSelected(false);
				getEmpiricRadio().setSelected(false);
				getNoneRadio().setSelected(true);
				getPoleRadio().setSelected(false);
				break;
			case POLE:
				getPhraseRadio().setSelected(false);
				getEmpiricRadio().setSelected(false);
				getNoneRadio().setSelected(false);
				getPoleRadio().setSelected(true);
				break;
			default:
				break;
		}
		
		getDescriptionField().setText(w.getDescription());
		getFieldSynonymous().setText(w.getSynonymousToString());
		getWordNameField().setText(w.getNome());
		
		getWordList().updateUI();
		refreshSearchCount();
		
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		
		waitingToSaveDefinition = false;
		waitingToSaveDescription = false;
		waitingToSaveWordName = false;
		waitingToSaveSynonymous = false;
		
		getDescriptionField().setEnabled(enabled);
		getNoneRadio().setEnabled(enabled);
		getPoleRadio().setEnabled(enabled);
		getEmpiricRadio().setEnabled(enabled);
		getPhraseRadio().setEnabled(enabled);
		getWordNameField().setEnabled(enabled);
		getFieldSynonymous().setEnabled(enabled);

		if(getWordGraphic() != null) {
			switch(getWordGraphic().getWord().getType()){
				case POLE:
					setEnabledMeanFields(true);
					changeMeanPane(POLE_PANE);
					break;
				case PHRASE:
					setEnabledMeanFields(true);
					changeMeanPane(DEFINITION_PANE);
					break;
				default:
					setEnabledMeanFields(false);
					changeMeanPane(DEFINITION_PANE);
					clearMeanFields();
					break;
			}
		}else
			setEnabledMeanFields(false);
		
		if(!enabled) clearFields();
	}
	
	public void clearFields() {
		getDescriptionField().setText("");
		getWordNameField().setText("");
		getDefinitionField().setText("");
		getFieldSynonymous().setText("");
	}

	@Override public void focusGained(FocusEvent e) {}

	public void fieldTypeEvent(DocumentEvent e) {
		if(e.getDocument() == getDefinitionField() || 
				e.getDocument() == getWrapField() ||
				e.getDocument() == getWrappedField()) 
			waitingToSaveDefinition = true;
		if(e.getDocument() == getDescriptionField())
			waitingToSaveDescription = true;
		if(e.getDocument() == getWordNameField()) {
			waitingToSaveWordName = true;
		}		
		if(e.getDocument() == getFieldSynonymous())
			waitingToSaveSynonymous = true;
	}
	
	@Override public void focusLost(FocusEvent e) {
		
		if(waitingToSaveDefinition) {
			getDefinitionField().setText(getDefinitionField().getText().replaceAll("\n", ""));
		}
		
		if(waitingToSaveDefinition || waitingToSaveDescription
				|| waitingToSaveWordName || waitingToSaveSynonymous)
			fieldChangeEvent((JTextComponent) e.getSource());
	}
	
	@Override public void keyTyped(KeyEvent e) {
		if(e.getSource() == getWordNameField() && 
				(!(e.getKeyChar()+"").matches("[a-zA-ZÀ-ú]+"))) {
			e.consume();
		}
	
		if(e.getSource() == getDefinitionField() && 
				(!(e.getKeyChar()+"").matches("[a-zA-ZÀ-ú ]+"))) {
			e.consume();
		}
		
		if(e.getSource() == getFieldSynonymous() && 
				(!(e.getKeyChar()+"").matches("[a-zA-ZÀ-ú ]+"))) {
			e.consume();
		}
		
		if((e.getSource() == getWrapField() || e.getSource() == getWrappedField()) && 
				(!(e.getKeyChar()+"").matches("[a-zA-ZÀ-ú]+"))) {
			e.consume();
		}
	}
	
	@Override public void keyReleased(KeyEvent e) {
		
		if(e.getSource() == getDefinitionField() || 
				e.getSource() == getWrapField() ||
				e.getSource() == getWrappedField()) 
			waitingToSaveDefinition = true;
		if(e.getSource() == getDescriptionField()) {
			waitingToSaveDescription = true;
		}
		if(e.getSource() == getWordNameField()) {
			waitingToSaveWordName = true;
		}
		if(e.getSource() == getFieldSynonymous()) {
			waitingToSaveSynonymous = true;
		}
		
		// Refresh focus instead call field change event
		// to call field change event using focus lost event
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			requestFocus();
			((JTextComponent)e.getSource()).requestFocus();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}

	@Override public void insertUpdate(DocumentEvent e) {doSearch();}
	@Override public void removeUpdate(DocumentEvent e) {doSearch();}
	@Override public void changedUpdate(DocumentEvent e) {doSearch();}

	public void doSearch() {
		WordListModel m = ((WordListModel)getWordList().getModel());
		m.setSearch(getSearchField().getText());
		getWordList().updateUI();
		Home home = IAWordBookEditor.HOME_PANEL;
		HierarchyPanel hi = home.getHierarchyPanel();
		if(m.getSize() != 0) {
			
			if(m.getElementAt(0).getHierarchy() instanceof WordGraphic)
				home.setSelectedWord(
						((WordGraphic)m.getElementAt(0).getHierarchy()).getWord());
			
			hi.scrollToFrame(m.getElementAt(0).getHierarchy());
		}
		
		refreshSearchCount();
		
		
	}
	
	public void refreshSearchCount() {

		WordListModel m = ((WordListModel)getWordList().getModel());
		refreshSearchCount(m.getSize());
	}
	
	public void refreshSearchCount(int i) {

		getSearchPanel().setTitle(String.format(SEARCH_PANEL_TITLE, i));
		getSearchPanel().repaint();
	}
	
}
