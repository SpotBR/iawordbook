package br.com.spotdev.iawordbook.test.form.wordedit;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.AbstractListModel;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyPanel;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.NoteInternalFrame;

public class WordListModel extends AbstractListModel<WordListItemModel>{

	private static final long serialVersionUID = 2777664971712136827L;
	private Mind mind = null;
	private String search = "";
	
	public WordListModel(Mind mind) {
		this.mind = mind;
		
	}
	
	@Override
	public WordListItemModel getElementAt(int arg0) {
		
		return getSortedList().get(arg0);
	}

	public int getWordIndex(WordListItemModel w) {
		List<WordListItemModel> words = getSortedList();
		if(words.contains(w)) {
			return words.indexOf(w);
		}
		
		return -1;
	}
	
	public List<WordListItemModel> getSortedList(){
		List<WordListItemModel> l = null;
		if(search.startsWith("?")) {
			l = getSortedQuestions();
		}else if(search.startsWith("!")) {
			l = getSortedEmptyNotes();
		}else {
			l = getSortedListWords();
			l.addAll(getSortedListNotes());
		}
		return l;
	}
	
	public List<WordListItemModel> getSortedEmptyNotes(){
		HierarchyPanel hi = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
		List<WordListItemModel> notes = new ArrayList<WordListItemModel>();
		List<WordListItemModel> toRemove = new ArrayList<>();
		for(NoteInternalFrame w : hi.getNotes()) {
			if(w.getJtext().getText().isEmpty()) {
				WordListItemModel wl = new WordListItemModel(w);
				notes.add(wl);
			}
		}
		search = search.trim().toLowerCase();
		String search = this.search.substring(1,this.search.length());
		for(WordListItemModel w : notes) {
			if(!w.toString().toLowerCase().startsWith(search) && 
					!(search.length() >= 3 && w.toString().toLowerCase().contains(search))) {
				toRemove.add(w);
			}
		}
		notes.removeAll(toRemove);
		notes.sort(new Comparator<WordListItemModel>() {
			@Override public int compare(WordListItemModel o1, WordListItemModel o2) {
				if((o1.toString().toLowerCase().startsWith(search) && 
						o2.toString().toLowerCase().startsWith(search)) ||
						(!o1.toString().toLowerCase().startsWith(search) && 
								!o2.toString().toLowerCase().startsWith(search)))
					return o1.toString().compareTo(o2.toString());
				else if(o1.toString().toLowerCase().startsWith(search) && 
						!o2.toString().toLowerCase().startsWith(search))
					return -1;
				else
					return 1;
			}
		});
		return notes;
	}

	

	public List<WordListItemModel> getSortedQuestions(){
		HierarchyPanel hi = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
		List<WordListItemModel> questions = new ArrayList<WordListItemModel>();
		List<WordListItemModel> toRemove = new ArrayList<>();
		for(NoteInternalFrame w : hi.getNotes()) {
			for(String question : w.getQuestions()) {
				WordListItemModel wl = new WordListItemModel(w);
				wl.setQuestion(question);
				questions.add(wl);
			}
		}
		search = search.trim().toLowerCase();
		String search = this.search.substring(1,this.search.length());
		for(WordListItemModel w : questions) {
			if(!w.toString().toLowerCase().startsWith(search) && 
					!(search.length() >= 3 && w.toString().toLowerCase().contains(search))) {
				toRemove.add(w);
			}
		}
		questions.removeAll(toRemove);
		questions.sort(new Comparator<WordListItemModel>() {
			@Override public int compare(WordListItemModel o1, WordListItemModel o2) {
				if((o1.toString().toLowerCase().startsWith(search) && 
						o2.toString().toLowerCase().startsWith(search)) ||
						(!o1.toString().toLowerCase().startsWith(search) && 
								!o2.toString().toLowerCase().startsWith(search)))
					return o1.toString().compareTo(o2.toString());
				else if(o1.toString().toLowerCase().startsWith(search) && 
						!o2.toString().toLowerCase().startsWith(search))
					return -1;
				else
					return 1;
			}
		});
		return questions;
	}

	
	public List<WordListItemModel> getSortedListNotes(){
		HierarchyPanel hi = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
		List<WordListItemModel> notes = new ArrayList<WordListItemModel>();
		List<WordListItemModel> toRemove = new ArrayList<>();
		for(NoteInternalFrame w : hi.getNotes()) notes.add(new WordListItemModel(w));
		search = search.trim().toLowerCase();
		if(!search.isEmpty()) {
			for(WordListItemModel w : notes) {
				if(!w.toString().toLowerCase().startsWith(search) && 
						!(search.length() >= 3 && w.toString().toLowerCase().contains(search))) {
					toRemove.add(w);
				}
			}
		}
		notes.removeAll(toRemove);
		notes.sort(new Comparator<WordListItemModel>() {
			@Override public int compare(WordListItemModel o1, WordListItemModel o2) {
				if((o1.toString().toLowerCase().startsWith(search) && 
						o2.toString().toLowerCase().startsWith(search)) ||
						(!o1.toString().toLowerCase().startsWith(search) && 
								!o2.toString().toLowerCase().startsWith(search)))
					return o1.toString().compareTo(o2.toString());
				else if(o1.toString().toLowerCase().startsWith(search) && 
						!o2.toString().toLowerCase().startsWith(search))
					return -1;
				else
					return 1;
			}
		});
		return notes;
	}
	
	public List<WordListItemModel> getSortedListWords(){
		HierarchyPanel hi = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
		List<Word> words = new ArrayList<Word>();
		List<Word> toRemove = new ArrayList<>();
		for(Word w : mind.getWords()) words.add(w);
		search = search.trim().toLowerCase();
		if(!search.isEmpty()) {
			for(Word w : words) {
				
				boolean hasSynon = false;
				for(String synon : w.getSynonymous()) {
					if(synon.toLowerCase().startsWith(search)) {
						hasSynon = true;
					}
				}
				
				if(!w.getNome().toLowerCase().startsWith(search) && !hasSynon) {
					toRemove.add(w);
				}
			}
		}
		words.removeAll(toRemove);
		words.sort(new Comparator<Word>() {
			@Override public int compare(Word o1, Word o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		
		List<WordListItemModel> hiWord = new ArrayList<WordListItemModel>();
		words.forEach((v)->{
			hiWord.add(new WordListItemModel(hi.getWordGraphicsByWord(v)));
		});
		return hiWord;
	}
	
	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	@Override
	public int getSize() { 
		return getSortedList().size();
	}

	public Mind getMind() {
		return mind;
	}

	public void setMind(Mind mind) {
		this.mind = mind;
	}

}
