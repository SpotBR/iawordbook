package br.com.spotdev.iawordbook.test.form;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.ServerSocket;

import javax.swing.JFrame;

public class CheckifIsOpenedSocket {
	
	public static final int PORT = 8989;
	public static ServerSocket SERVER = null;
	
	public static boolean isAlreadyOpenned() {
		
		try {
			SERVER = new ServerSocket(PORT);
		} catch (IOException e) {
			return true;
		}
		
		return false;
	}
	
	public static void setFrameToCloseServer(JFrame frame) {
		frame.addWindowListener(new WindowListener() {

			@Override public void windowDeiconified(WindowEvent e) {}
			@Override public void windowOpened(WindowEvent e) {}
			@Override public void windowIconified(WindowEvent e) {}
			@Override public void windowDeactivated(WindowEvent e) {}
			@Override public void windowClosing(WindowEvent e) {
				try {
					SERVER.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			@Override public void windowClosed(WindowEvent e) {}
			@Override public void windowActivated(WindowEvent e) {}
		});
	}

}
