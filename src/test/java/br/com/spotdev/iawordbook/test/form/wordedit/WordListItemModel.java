package br.com.spotdev.iawordbook.test.form.wordedit;

import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyInternalFrame;

public class WordListItemModel {

	private HierarchyInternalFrame hierarchy = null;
	private String question = null;
	
	public WordListItemModel(HierarchyInternalFrame hi) {
		hierarchy = hi;
	}
	
	public HierarchyInternalFrame getHierarchy() {
		return hierarchy;
	}
	public void setHierarchy(HierarchyInternalFrame hierarchy) {
		this.hierarchy = hierarchy;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public boolean isQuestion() {
		return question != null;
	}
	
	@Override
	public String toString() {
		if(isQuestion()) {
			return getQuestion()+" (Question) ";
		}else {
			return getHierarchy().toString();
		}
	}
	
}
