package br.com.spotdev.iawordbook.test.form.wordcomponents;

import java.awt.Graphics;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.Border;

import br.com.spotdev.iawordbook.Word;

public abstract class AbstractDefinitionWordGraphic extends WordGraphic {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3961469352279615887L;

	public AbstractDefinitionWordGraphic(Word word) {
		super(word);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		getMeanBlocks().forEach((v)-> {
			v.repaint();
		});
	}
	
	public WordBlock getBlockByWord(Word w) {
		for(WordBlock wb : getMeanBlocks()) {
			if(w == (wb.getWord()))
				return wb;
		}
		
		return null;
	}

	public abstract List<WordBlock> getMeanBlocks();
	public abstract WordBlock addWordBlock(Word w, JPanel p);
	@Override public abstract String getTitleTag();
	@Override public abstract Border getBorderStyle();
	

}
