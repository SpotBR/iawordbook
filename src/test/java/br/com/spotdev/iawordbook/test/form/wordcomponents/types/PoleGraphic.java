package br.com.spotdev.iawordbook.test.form.wordcomponents.types;

import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.mean.types.PoleMean;
import br.com.spotdev.iawordbook.test.form.wordcomponents.AbstractDefinitionWordGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordBlock;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import java.awt.FlowLayout;

public class PoleGraphic extends AbstractDefinitionWordGraphic {

	
	private static final long serialVersionUID = -7137638155715150013L;
	
	private WordBlock wrap = null;
	private WordBlock wrapped = null;
	
	public PoleGraphic(Word word) {
		super(word);

		getWordPanel().setBackground(new Color(153, 235, 255));
		
		JPanel meanPanel = new JPanel();
		meanPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
		getContentPane().add(meanPanel, BorderLayout.SOUTH);
		FlowLayout fl = (FlowLayout) meanPanel.getLayout();
		fl.setHgap(0);
		fl.setVgap(0);
		
		PoleMean pm = (PoleMean) word.getSelectedMean();
		addLabel("F", meanPanel);
		WordBlock wbF = addWordBlock(pm.getWrap(), meanPanel);
		WordBlock wbM = addWordBlock(pm.getWrapped(), meanPanel);
		addLabel("M", meanPanel);
		
		this.setWrap(wbF);
		this.setWrapped(wbM);
		
		this.pack();
	}
	
	private void addLabel(String title, JPanel p) {
		JLabel wordLabel_1 = new JLabel(title);
		wordLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel jp = new JPanel();
//		jp.setBackground(Color.WHITE);
		jp.setBorder(new BevelBorder(BevelBorder.LOWERED));
		jp.add(wordLabel_1);

		p.add(jp);
	}
	
	@Override
	public WordBlock addWordBlock(Word w, JPanel p) {
		String label = w == null ? "(None)" : w.getNome();
		JLabel wordLabel_1 = new JLabel(label.toLowerCase());
		wordLabel_1.setFont(new Font("Helvetica", Font.PLAIN, 7));
		wordLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		WordBlock jp = new WordBlock(w,wordLabel_1);
		jp.setBorder(new BevelBorder(BevelBorder.LOWERED));
		jp.add(wordLabel_1);

		p.add(jp);
		return jp;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		getMeanBlocks().forEach((v)-> {
			v.repaint();
		});
	}
	
	@Override
	public WordBlock getBlockByWord(Word w) {
		for(WordBlock wb : getMeanBlocks()) {
			if(w == (wb.getWord()))
				return wb;
		}
		
		return null;
	}
	
	public WordBlock getWrap() {
		return wrap;
	}

	public void setWrap(WordBlock wrap) {
		this.wrap = wrap;
	}

	public WordBlock getWrapped() {
		return wrapped;
	}

	public void setWrapped(WordBlock wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public List<WordBlock> getMeanBlocks() {
		List<WordBlock> block = new ArrayList<>();
		block.add(wrap);
		block.add(wrapped);
		return block;
	}

	@Override
	public String getTitleTag() {
		return "PL";
	}

	@Override
	public Border getBorderStyle() {
		return new BevelBorder(BevelBorder.RAISED,Color.GRAY,Color.GRAY);
	}

}
