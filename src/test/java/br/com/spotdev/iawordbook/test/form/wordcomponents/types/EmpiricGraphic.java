package br.com.spotdev.iawordbook.test.form.wordcomponents.types;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordGraphic;

import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

public class EmpiricGraphic extends WordGraphic {

	
	private static final long serialVersionUID = -7137638155715150013L;
	
	
	public EmpiricGraphic(Word word) {
		super(word);
		getWordPanel().setBackground(new Color(153, 255, 153));
		
		this.pack();
		getWordPanel().repaint();
	}


	@Override
	public String getTitleTag() {
		return "EM";
	}


	@Override
	public Border getBorderStyle() {
		return new BevelBorder(BevelBorder.RAISED,Color.GRAY,Color.GRAY);
	}

}
