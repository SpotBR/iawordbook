package br.com.spotdev.iawordbook.test.form.hierarchy;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.plaf.basic.BasicInternalFrameUI;

import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordHasPatternDependence;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.wordcomponents.WordGraphic;
import br.com.spotdev.iawordbook.test.form.wordcomponents.types.NoteInternalFrame;

public abstract class HierarchyInternalFrame extends JInternalFrame {

	private static final long serialVersionUID = -1068803848437537320L;
	
	private int lastPosX = 0;
	private int lastPosY = 0;
	
	private JPopupMenu popUpMenu = null;
	private MouseDragEvent mouseEvent = null;
	
	public HierarchyInternalFrame() {
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.setTitle(getTitleTag());
		Image icon = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB_PRE);
		this.setFrameIcon(new ImageIcon(icon));
		
		getNorthPanel().getComponent(0).setVisible(false);
		getNorthPanel().setCursor(new Cursor(Cursor.MOVE_CURSOR));
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JMenuItem deleteMenuItem = new JMenuItem("X Delete Word");
		popUpMenu = new JPopupMenu();
		popUpMenu.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentAdded(ContainerEvent e) {
				if(e.getChild() != deleteMenuItem)
					popUpMenu.setComponentZOrder(deleteMenuItem, 
							popUpMenu.getComponentCount()-1);
			}
		});
		deleteMenuItem.addActionListener(this::onPressDeleteItemMenu);
		popUpMenu.add(deleteMenuItem);
		
		new Thread(new SaveWordPosEvent(this)).start();
		this.addComponentListener(new ComponentEvent(this));
		
		List<Component> blackList = new ArrayList<>();
		for(Component c : getNorthPanel().getComponents())blackList.add(c);
		blackList.add(getNorthPanel());
		mouseEvent = new MouseDragEvent(this);	
		for(Component c : this.getComponents()) {
			if(blackList.contains(c))
				continue;
			
			c.addMouseMotionListener(mouseEvent);
			c.addMouseListener(mouseEvent);
		}
	}
	
	public MouseDragEvent getMouseEvent() {
		return mouseEvent;
	}

	public JPopupMenu getPopUpMenu() {
		return popUpMenu;
	}

	public void onPressDeleteItemMenu(ActionEvent ae) {
		HierarchyPanel hp = (HierarchyPanel) this.getParent(); 
		try {
			hp.removeWordAndGraphic(this);
		} catch (WordHasPatternDependence e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), 
					"Delete Word", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), 
					"Erro", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private class ComponentEvent implements ComponentListener {
		HierarchyInternalFrame wg = null;
		
		public ComponentEvent(HierarchyInternalFrame wg) {this.wg = wg;}
		
		@Override public void componentShown(java.awt.event.ComponentEvent e) {}
		@Override public void componentResized(java.awt.event.ComponentEvent e) {}
		@Override public void componentHidden(java.awt.event.ComponentEvent e) {}
		
		@Override public void componentMoved(java.awt.event.ComponentEvent e) {
			
			if(wg.isVisible()) {
				HierarchyPanel h = (HierarchyPanel) wg.getParent();
				h.checkSize(wg);
			}
		}
			
	}
	
	private class SaveWordPosEvent implements Runnable {

		HierarchyInternalFrame wg = null;
		
		public SaveWordPosEvent(HierarchyInternalFrame wg) {
			this.wg = wg;
		}
		
		@Override
		public void run() {
			while(true) {
				try {Thread.sleep(1000);} catch (InterruptedException e1) {}
				
				if(wg.isVisible() && IAWordBookEditor.
						MAIN_FRAME.isVisible()) {
					int x = wg.getX();
					int y = wg.getY();
					
					if(x != lastPosX || y != lastPosY) {
						lastPosX = wg.getX();
						lastPosY = wg.getY();
						if(!save())
							break;
					}
				}
				
				
			}
		}
		
	}
	
	private class MouseDragEvent implements MouseListener, MouseMotionListener {
		
		HierarchyInternalFrame hif = null;
		public MouseDragEvent(HierarchyInternalFrame hif) {this.hif = hif;}

		@SuppressWarnings("unchecked")
		@Override public void mouseDragged(MouseEvent e) {
			int x = e.getLocationOnScreen().x;
			int y = e.getLocationOnScreen().y;
			int wgX = (int) hif.getLocationOnScreen().getX()+hif.getWidth()/2;
			int wgY = (int) hif.getLocationOnScreen().getY()+hif.getHeight()/2;
			
			HierarchyPanel hp = (HierarchyPanel) hif.getParent();
			int xDirection = x-wgX;
			int yDirection = y-wgY;
			
			ArrayList<JInternalFrame> moveList = new ArrayList<>();
			
			if(hif instanceof WordGraphic) {
				WordGraphic wg = (WordGraphic) hif;
				moveList.addAll(hp.getAllAbove(wg));
				
				moveList.removeIf(new Predicate<JInternalFrame>() {
					
					ArrayList<WordGraphic> isChildOfRemovedParent = 
							new ArrayList<>();
					@Override public boolean test(JInternalFrame childJi) {
						WordGraphic child = (WordGraphic)childJi;
						Mind mind = IAWordBookEditor.HOME_PANEL.getHierarchyPanel().
								getMind();
						Word parents[] = mind.getPattern(child.getWord());
						boolean isMultipleParent = parents.length > 1;
						if(isChildOfRemovedParent.contains(child) || 
								isMultipleParent) {
							if(isMultipleParent) {
								isChildOfRemovedParent.addAll(hp.getAllAbove(child));
							}
							return true;
						}
						
						return false;
					}
				});
				
				
				((ArrayList<WordGraphic>) moveList.clone()).forEach((v)-> {
					for(NoteInternalFrame nf : 
						hp.getNoteByWord(v.getWord())) {
						moveList.add(nf);
					}
				});
				moveList.addAll(hp.getNoteByWord(wg.getWord()));
			}
			
			moveList.add(hif);
			JInternalFrame[] wgL = moveList.toArray(new JInternalFrame[moveList.size()]);
			
			hp.setBulkMovement(xDirection, yDirection, wgL);
		}	

		@Override public void mousePressed(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON3) {
				getPopUpMenu().show((Component)e.getSource(), e.getX(), e.getY());
			}
			
			onPressOnObject(e);
		}

		@Override public void mouseClicked(MouseEvent e) {}
		@Override public void mouseReleased(MouseEvent e) {}
		@Override public void mouseMoved(MouseEvent e) {}
		@Override public void mouseEntered(MouseEvent e) {}
		@Override public void mouseExited(MouseEvent e) {}
		
	}

	public abstract void onPressOnObject(MouseEvent e);
	public abstract String getTitleTag();
	public abstract boolean save();
	public abstract void removeSavedData() throws Exception;
	
	public JComponent getNorthPanel() {
		return ((BasicInternalFrameUI) this.getUI()).getNorthPane();
	}
	
	@Override
	public String toString() {
		if(this instanceof NoteInternalFrame) {
			return getTitle()+" (NOTE)";
		}else if(this instanceof WordGraphic){
			String wName = ((WordGraphic)this).getWord().
					getNome();
			return wName.substring(0, 1).toUpperCase() + 
					wName.substring(1)+" (WORD)";
		}else {
			return getTitle();
		}
	}
}
