package br.com.spotdev.iawordbook.test.form.wordedit;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ListSelectionModel;

import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.test.form.WhiteTitledPane;

public class AbstractWordEditPanel extends JPanel {

	private static final long serialVersionUID = 4166317072736570615L;
	
	public static final int DEFINITION_PANE = 0;
	public static final int POLE_PANE = 1;
	
	private JRadioButton phraseRadio;
	private JRadioButton empiricRadio;
	private JRadioButton noneRadio;
	private JRadioButton poleRadio;
	
	private JList<WordListItemModel> wordList;
	private JScrollPane searchScrollPane;
	
	private JTextArea descriptionField;
	private JTextField wordNameField;
	private JTextField searchField;
	private JTextArea definitionField;
	
	private Mind mind = null;
	private JLayeredPane layeredPane;
	private JPanel definitionPanel;
	private JLabel lblWrap;
	private JTextField wrapField;

	private JPanel polePanel;
	private JTextField wrappedField;
	private JTextField synonymousField;
	
	private WhiteTitledPane searchPanel;
	public static final String SEARCH_PANEL_TITLE = "Search (type '?' or '!' for more) - %d";

	public AbstractWordEditPanel(Mind mind) {
		this.mind = mind;
		JPanel leftPanel = new WhiteTitledPane("Word Edit");
		searchPanel = new WhiteTitledPane("");
		JPanel wordDataPanel = new WhiteTitledPane("Word Data");
		
		JPanel meanPanel = new WhiteTitledPane("Word Mean");	
		this.setBackground(Color.WHITE);
		this.setSize(300,getHeight());
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
					.addComponent(leftPanel, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, getWidth(), Short.MAX_VALUE)
					.addComponent(searchPanel, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, getWidth(), Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(leftPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addComponent(searchPanel, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE))
		);
		
		GroupLayout gl_leftPanel = new GroupLayout(leftPanel);
		gl_leftPanel.setAutoCreateGaps(true);
		gl_leftPanel.setHorizontalGroup(
			gl_leftPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(wordDataPanel, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 
						GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
				.addComponent(meanPanel)
		);
		gl_leftPanel.setVerticalGroup(
			gl_leftPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_leftPanel.createSequentialGroup()
					.addComponent(wordDataPanel)
					.addComponent(meanPanel))
		);
		
		layeredPane = new JLayeredPane();
		GroupLayout gl_meanPanel = new GroupLayout(meanPanel);
		gl_meanPanel.setHorizontalGroup(
			gl_meanPanel.createParallelGroup(Alignment.TRAILING)
				.addComponent(layeredPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
		);
		gl_meanPanel.setVerticalGroup(
			gl_meanPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(layeredPane, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
		);
		
		definitionPanel = new JPanel();
		definitionPanel.setBackground(Color.WHITE);
		
		JLabel definitionLabel = new JLabel("Definition:");
		
		definitionField = new JTextArea();
		definitionField.setLineWrap(true);
		layeredPane.setLayer(definitionPanel, 0);
		GroupLayout gl_definitionPanel = new GroupLayout(definitionPanel);
		gl_definitionPanel.setAutoCreateGaps(true);
		gl_definitionPanel.setHorizontalGroup(
			gl_definitionPanel.createParallelGroup(Alignment.LEADING)
					.addComponent(definitionLabel)
					.addComponent(definitionField)
		);
		gl_definitionPanel.setVerticalGroup(
			gl_definitionPanel.createSequentialGroup()
					.addComponent(definitionLabel)
					.addComponent(definitionField)
		);
		definitionPanel.setLayout(gl_definitionPanel);
		
		polePanel = new JPanel();
		polePanel.setBackground(Color.WHITE);
		layeredPane.setLayer(polePanel, 1);
		
		lblWrap = new JLabel("Wrap:");
		
		wrapField = new JTextField();
		wrapField.setColumns(10);
		
		JLabel lblWrapped = new JLabel("Wrapped:");
		
		wrappedField = new JTextField();
		wrappedField.setColumns(10);
		GroupLayout gl_polePanel = new GroupLayout(polePanel);
		gl_polePanel.setAutoCreateGaps(true);
		gl_polePanel.setHorizontalGroup(
			gl_polePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_polePanel.createParallelGroup()
							.addComponent(lblWrap)
							.addComponent(wrapField))
							.addComponent(lblWrapped)
							.addComponent(wrappedField)
		);
		gl_polePanel.setVerticalGroup(
			gl_polePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_polePanel.createSequentialGroup()
							.addComponent(lblWrap)
							.addComponent(wrapField)
							.addComponent(lblWrapped)
							.addComponent(wrappedField))
		);
		polePanel.setLayout(gl_polePanel);
		layeredPane.setBackground(Color.WHITE);
		GroupLayout gl_layeredPane = new GroupLayout(layeredPane);
		gl_layeredPane.setAutoCreateGaps(true);
		gl_layeredPane.setAutoCreateContainerGaps(true);
		gl_layeredPane.setHorizontalGroup(
			gl_layeredPane.createParallelGroup()
				.addComponent(polePanel)
				.addComponent(definitionPanel)
		);
		gl_layeredPane.setVerticalGroup(
			gl_layeredPane.createParallelGroup()
				.addComponent(polePanel)
				.addComponent(definitionPanel, Alignment.LEADING)
		);
		layeredPane.setLayout(gl_layeredPane);
		meanPanel.setLayout(gl_meanPanel);
		
		JLabel wordNameLabel = new JLabel("Word Name:");
		
		wordNameField = new JTextField();
		
		JLabel descriptionLabel = new JLabel("Description:");
		
		descriptionField = new JTextArea();
		descriptionField.setLineWrap(true);
		descriptionField.setBorder(wordNameField.getBorder());
		descriptionField.setMinimumSize(new Dimension(
				0,60));
		
		JLabel meanTypeRadio = new JLabel("Mean Type:");
		
		phraseRadio = new JRadioButton("Phrase");
		phraseRadio.setBackground(Color.WHITE);
		
		empiricRadio = new JRadioButton("Empiric");
		empiricRadio.setBackground(Color.WHITE);
		
		noneRadio = new JRadioButton("None");
		noneRadio.setBackground(Color.WHITE);
		
		poleRadio = new JRadioButton("Pole");
		poleRadio.setBackground(Color.WHITE);
		
		JLabel labelSynonymous = new JLabel("Synonymous:");
		
		synonymousField = new JTextField();
		GroupLayout gl_wordDataPanel = new GroupLayout(wordDataPanel);
		gl_wordDataPanel.setAutoCreateGaps(true);
		gl_wordDataPanel.setAutoCreateContainerGaps(true);
		gl_wordDataPanel.setHorizontalGroup(
			gl_wordDataPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_wordDataPanel.createParallelGroup()
								.addComponent(meanTypeRadio))
					    .addGroup(gl_wordDataPanel.createSequentialGroup()
							.addGroup(gl_wordDataPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(phraseRadio)
								.addComponent(empiricRadio))
							.addGroup(gl_wordDataPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(poleRadio)
								.addComponent(noneRadio)))
						.addGroup(gl_wordDataPanel.createParallelGroup()
							.addComponent(labelSynonymous)
							.addComponent(synonymousField))
						.addGroup(gl_wordDataPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(descriptionLabel)
							.addComponent(wordNameLabel))
						.addGroup(gl_wordDataPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(descriptionField)
							.addComponent(wordNameField))
		);
		gl_wordDataPanel.setVerticalGroup(
			gl_wordDataPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_wordDataPanel.createSequentialGroup()
					.addGroup(gl_wordDataPanel.createSequentialGroup()
						.addComponent(wordNameLabel)
						.addComponent(wordNameField))
					.addGroup(gl_wordDataPanel.createSequentialGroup()
						.addComponent(descriptionLabel)
						.addComponent(descriptionField))
						.addComponent(meanTypeRadio)
					.addGroup(gl_wordDataPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(gl_wordDataPanel.createSequentialGroup()
							.addGroup(gl_wordDataPanel.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(phraseRadio)
								.addComponent(poleRadio))
							.addGroup(gl_wordDataPanel.createParallelGroup(GroupLayout.Alignment.BASELINE)
								.addComponent(empiricRadio)
								.addComponent(noneRadio))))
					.addGroup(gl_wordDataPanel.createSequentialGroup()
						.addComponent(labelSynonymous)
						.addComponent(synonymousField)))
		);
		wordDataPanel.setLayout(gl_wordDataPanel);
		leftPanel.setLayout(gl_leftPanel);
		
		searchField = new JTextField();
		searchField.setColumns(10);
		
		searchScrollPane = new JScrollPane();
		GroupLayout gl_searchPanel = new GroupLayout(searchPanel);
		gl_searchPanel.setHorizontalGroup(
			gl_searchPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(searchField)
				.addComponent(searchScrollPane)
		);
		gl_searchPanel.setVerticalGroup(
			gl_searchPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_searchPanel.createSequentialGroup()
					.addComponent(searchField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(searchScrollPane, GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
		);

		definitionField.setBorder(searchField.getBorder());
		
		wordList = new JList<WordListItemModel>();
		wordList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		wordList.setModel(new WordListModel(mind));
		searchScrollPane.setViewportView(wordList);
		searchPanel.setLayout(gl_searchPanel);
		setLayout(groupLayout);
		
	}
	
	public void clearMeanFields(){
		this.getDefinitionField().setText("");
		this.getWrappedField().setText("");
		this.getWrapField().setText("");
	}
	
	public void setEnabledMeanFields(boolean enabled){
		if(enabled){
			this.getDefinitionField().setEnabled(true);
			this.getWrapField().setEnabled(true);
			this.getWrappedField().setEnabled(true);
		}else{
			this.getDefinitionField().setEnabled(false);
			this.getWrapField().setEnabled(false);
			this.getWrappedField().setEnabled(false);
		}
	}

	public void changeMeanPane(int pane_id){
		if(pane_id == DEFINITION_PANE){
			layeredPane.setLayer(definitionPanel, 1);
			layeredPane.setLayer(polePanel, 0);
		}else if(pane_id == POLE_PANE){
			layeredPane.setLayer(definitionPanel, 0);
			layeredPane.setLayer(polePanel, 1);
		}
	}
	
	public JTextField getFieldSynonymous() {
		return synonymousField;
	}

	public JRadioButton getPhraseRadio() {
		return phraseRadio;
	}

	public void setPhraseRadio(JRadioButton phraseRadio) {
		this.phraseRadio = phraseRadio;
	}

	public JRadioButton getEmpiricRadio() {
		return empiricRadio;
	}

	public void setEmpiricRadio(JRadioButton empiricRadio) {
		this.empiricRadio = empiricRadio;
	}

	public JRadioButton getNoneRadio() {
		return noneRadio;
	}

	public void setNoneRadio(JRadioButton noneRadio) {
		this.noneRadio = noneRadio;
	}

	public JList<?> getWordList() {
		return wordList;
	}

	public void setWordList(JList<WordListItemModel> wordList) {
		this.wordList = wordList;
	}

	public JScrollPane getSearchScrollPane() {
		return searchScrollPane;
	}

	public void setSearchScrollPane(JScrollPane searchScrollPane) {
		this.searchScrollPane = searchScrollPane;
	}

	public JTextArea getDescriptionField() {
		return descriptionField;
	}

	public void setDescriptionField(JTextArea descriptionField) {
		this.descriptionField = descriptionField;
	}

	public JTextField getWordNameField() {
		return wordNameField;
	}

	public void setWordNameField(JTextField wordNameField) {
		this.wordNameField = wordNameField;
	}

	public JRadioButton getPoleRadio() {
		return poleRadio;
	}

	public void setPoleRadio(JRadioButton poleRadio) {
		this.poleRadio = poleRadio;
	}

	public JTextField getSearchField() {
		return searchField;
	}

	public void setSearchField(JTextField searchField) {
		this.searchField = searchField;
	}

	public JTextArea getDefinitionField() {
		return definitionField;
	}

	public void setDefinitionField(JTextArea definitionField) {
		this.definitionField = definitionField;
	}
	
	public Mind getMind() {
		return mind;
	}

	public void setMind(Mind mind) {
		this.mind = mind;
	}

	public JTextField getWrapField() {
		return wrapField;
	}

	public void setWrapField(JTextField wrapField) {
		this.wrapField = wrapField;
	}

	public JTextField getWrappedField() {
		return wrappedField;
	}

	public void setWrappedField(JTextField wrappedField) {
		this.wrappedField = wrappedField;
	}

	public WhiteTitledPane getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(WhiteTitledPane searchPanel) {
		this.searchPanel = searchPanel;
	}
	
}
