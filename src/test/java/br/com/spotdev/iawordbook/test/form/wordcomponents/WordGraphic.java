package br.com.spotdev.iawordbook.test.form.wordcomponents;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import br.com.spotdev.iawordbook.Word;
import br.com.spotdev.iawordbook.database.WordLoader;
import br.com.spotdev.iawordbook.mind.Mind;
import br.com.spotdev.iawordbook.mind.WordHasPatternDependence;
import br.com.spotdev.iawordbook.test.IAWordBookEditor;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyInternalFrame;
import br.com.spotdev.iawordbook.test.form.hierarchy.HierarchyPanel;

public abstract class WordGraphic extends HierarchyInternalFrame  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6181639006447826598L;
	
	public static final int WORD_FONT_SIZE = 12;
	
	private Word word = null;
	private boolean forcedSelect = false;
	private JPanel wordPanel = null;
	
	public WordGraphic(Word word) {
		super();
		
		refreshBoder();
		wordPanel = new JPanel();
		getContentPane().add(wordPanel, BorderLayout.CENTER);
		wordPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel wordLabel = new JLabel(word.getNome());
		wordLabel.setFont(new Font("Helvetica", Font.BOLD, WORD_FONT_SIZE));
		wordLabel.setHorizontalAlignment(SwingConstants.CENTER);
		wordPanel.add(wordLabel);
		
		int bottomBorder = 10;
		if(word.getSynonymous().length != 0) {
			bottomBorder = 0;
			StringBuilder sb = new StringBuilder();
			sb.append("\n("+word.getSynonymous()[0]);
			for(int i = 1; i < word.getSynonymous().length; i++) {
				sb.append(", ");
				sb.append(word.getSynonymous()[i]);
			}
			sb.append(")");
			
			JLabel wordSinonymous = new JLabel(sb.toString());
			wordSinonymous.setFont(new Font("Helvetica", Font.BOLD, WORD_FONT_SIZE));
			wordSinonymous.setBorder(new EmptyBorder(0, 0, 10, 0));
			wordSinonymous.setHorizontalAlignment(SwingConstants.CENTER);
			wordPanel.add(wordSinonymous);
		}

		wordLabel.setBorder(new EmptyBorder(10, 20, bottomBorder, 20));
		
		this.word = word;

		JMenuItem generateDefinitionsMenuItem = new JMenuItem("? Generate Definitions");
		generateDefinitionsMenuItem.addActionListener(this::onPressGenerateDefItemMenu);
		
		this.getContentPane().add(getWordPanel());
		
		List<Component> blackList = new ArrayList<>();
		for(Component c : getNorthPanel().getComponents())blackList.add(c);
		blackList.add(getNorthPanel());
		for(Component c : blackList) {
			c.addMouseListener(new MouseListener() {
				@Override public void mouseReleased(MouseEvent arg0) {}
				@Override public void mousePressed(MouseEvent arg0) {
					IAWordBookEditor.HOME_PANEL.setSelectedWord(getWord());
				}
				@Override public void mouseExited(MouseEvent arg0) {}
				@Override public void mouseEntered(MouseEvent arg0) {}
				@Override public void mouseClicked(MouseEvent arg0) {}
			});
		}
		

		getPopUpMenu().add(generateDefinitionsMenuItem);
	}
	

	
	public boolean isForcedSelect() {
		return forcedSelect;
	}

	@Override
	public boolean save() {
		String[] posX = {"posx",this.getX()+""};
		String[] posY = {"posy",this.getY()+""};
		
		if(!IAWordBookEditor.HOME_PANEL.getHierarchyPanel().getMind().contains(getWord()))
			return false;
		
		WordLoader.saveWord(getWord(), posX, posY);
		return true;
	}

	private void setForcedSelect(boolean forcedSelect) {
		this.forcedSelect = forcedSelect;
	}

	public void onPressGenerateDefItemMenu(ActionEvent ae) {
		IAWordBookEditor.showOnlineDefinitionFrame(getWord());
	}
	
	public boolean hasPosParam() {
		if(getWord().getParam("posx") != null && getWord().getParam("posy") != null)
			return true;
		
		return false;
	}
		
	public Point getPosParam() {
		int x = Integer.parseInt(getWord().getParam("posx"));
		int y = Integer.parseInt(getWord().getParam("posy"));
		return new Point(x,y);
	}
	
	public JPanel getWordPanel() {
		return wordPanel;
	}

	public void setWordPanel(JPanel wordPanel) {
		this.wordPanel = wordPanel;
	}

	public Word getWord() {
		return word;
	}

	public void setWord(Word word) {
		this.word = word;
	}
	
	public void refreshBoder() {
		if(this.isSelected())
			this.setBorder(new LineBorder(Color.BLACK,2));
		else
			this.setBorder(getBorderStyle());
		
	}
	
	@Override
	public boolean isSelected() {
		return isForcedSelect();
	}
	
	@Override public void setSelected(boolean selected) throws PropertyVetoException {
		setForcedSelect(selected);
		if(selected) {
			HierarchyPanel hi = IAWordBookEditor.HOME_PANEL == null ? 
					(HierarchyPanel) this.getParent() : 
						IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
			for(WordGraphic wg : hi.getWordGraphics()) {
				if(wg != this) {
					wg.setForcedSelect(false);
					wg.updateUI();
				}
			}
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		refreshBoder();
	}
	
	public abstract Border getBorderStyle();
	
	@Override
	public synchronized boolean isVisible() {
		return super.isVisible();
	}
	
	@Override
	public void onPressOnObject(MouseEvent e) {
		if(e.getButton() != MouseEvent.BUTTON3) {
			IAWordBookEditor.HOME_PANEL.setSelectedWord(getWord());
		}
	}
	
	@Override
	public void removeSavedData() throws WordHasPatternDependence, 
		IOException {
		HierarchyPanel hi = IAWordBookEditor.HOME_PANEL.getHierarchyPanel();
		Mind m = hi.getMind();
		m.removeWord(getWord());
	}
	
	
}
